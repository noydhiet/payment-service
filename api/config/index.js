/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/10/2020
 */
require('dotenv').config();
const confidence = require('confidence');

const config = {
    port: process.env.PORT,
    authorization: {
        host: process.env.AUTHORIZATION_API_HOST,
        endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
        token: process.env.AUTHORIZATION_TOKEN,
        timeOut: process.env.AUTHORIZATION_TIMEOUT
    },
    authentication: {
        token: process.env.SHARED_SECRET
    },
    basicAuthApi: [
        {
            username: process.env.BASIC_AUTH_USERNAME,
            password: process.env.BASIC_AUTH_PASSWORD
        }
    ],
    elasticSearch: {
        size: process.env.ELASTICSEARCH_MAX_SIZE,
    },
    userProfileService: {
        host: process.env.USER_PROFILE_SERVICE_HOST,
        getUserAccountEndpoint: process.env.USER_PROFILE_GET_USER_ACCOUNT_ENDPOINT,
        getUserProfileEndpoint: process.env.USER_PROFILE_GET_USER_PROFILE_ENDPOINT,
        getUserInboxEndpoint:process.env.INBOX_PUSH_NOTIFICATION_ENDPOINT,
        sendHomeNotificationEndpoint:process.env.HOME_PUSH_NOTIFICATION_ENDPOINT,
        rewardsActiveStatusEndpoint: process.env.USER_PROFILE_REWARDS_STATUS_ENDPOINT,
    },
    rewardService: {
        getPointsRedeemHistoryEndpoint: process.env.REWARDS_GET_POINTS_REDEEM_HISTORY,
        getPointsHistoryEndpoint: process.env.REWARDS_GET_POINTS_HISTORY,
        getPointsListEndpoint: process.env.REWARDS_GET_POINTS_LIST,
        getPointDetailsEndpoint: process.env.REWARDS_GET_POINT_DETAIL,
        checkPointsEndpoint: process.env.REWARDS_CHECK_POINTS,
        redeemPointsEndpoint: process.env.REWARDS_REDEEM_POINTS,
        useVoucherEndpoint: process.env.REWARDS_USE_VOUCHER,
        getVouchersEndpoint: process.env.REWARDS_GET_VOUCHERS,
        rewardsESIndex: process.env.ELASTICSEARCH_REWARDS_INDEX,
        rewardsPreferencesESIndex: process.env.ELASTICSEARCH_REWARDS_PREFERENCES_INDEX,
        rewardsTypesESIndex: process.env.ELASTICSEARCH_REWARDS_TYPES_INDEX,
    },
    paymentService: {
        getFinpayReturnEndpoint: process.env.FINPAY_RETURN_URL,
        getFinpaySuccessEndpoint: process.env.FINPAY_SUCCESS_URL,
        getFinpayFaliureEndpoint: process.env.FINPAY_FALIURE_URL,
        getEsPaymentIndex:process.env.ELASTICSEARCH_PAYMENT_RECORDS_INDEX,
        getEsPaymentPackageDetails:process.env.ELASTICSEARCH_PAYMENT_PACKAGE_INDEX,
        getEsPaymenttype:process.env.ELASTICSEARCH_PAYMENT_TYPE,
        getDepositEndpoint:process.env.MANAGE_DEPOSIT_URL,
        getEsRefundIndex:process.env.ELASTICSEARCH_REFUND_INDEX,
        getEsRefundType:process.env.ELASTICSEARCH_REFUND_TYPE,
    },
    ssoService:{
        host: process.env.IDENTITY_BASE_URL
    },
    tMoney: {
        tmoneyLoginEndpoint: process.env.TMONEY_LOGIN,
        tmoneyChangePinEndpoint: process.env.TMONEY_CHANGE_PIN,
        tmoneyResetPinEndpoint: process.env.TMONEY_RESET_PIN,
        tmoneyTansferToLinkaja: process.env.TMONEY_TRANFER_TO_LINKAJA,
        tmoneyEmailCheck: process.env.TMONEY_EMAIL_CHECK,
        tmoneySignUp: process.env.TMONEY_SIGN_UP,
        getTmoneyReturnEnapoint:process.env.TMONEY_RETURN_URL,
        tMoneyPostPaidEndpoint:process.env.TMONEY_POSTPAID_URL
    },
    billing:{
        getOutstandingBill:process.env.TMONEY_OUTSTANDING_BILL,
        getBillDetails:process.env.TMONEY_BILL_DETAILS,
        getTmoneyReturnEnapoint: process.env.TMONEY_RETURN_URL,
        clientEmailSenderApi: process.env.CLIENT_EMAIL_API,
        emailSenderAddress: process.env.CLIENT_EMAIL_SENDER
    },
    subscriptionService: {
        host: process.env.SUBSCRIPTION_SERVICE_HOST,
        getSoDSubscriptionEndpoint: process.env.SUBSCRIPTION_SERVICE_GET_SOD_SUBSCRIPTION_ENDPOINT,
        activateSoDEndpoint: process.env.SUBSCRIPTION_SERVICE_ACTIVATE_SOD_ENDPOINT,
        getInternetSubscriptionEndpoint: process.env.SUBSCRIPTION_SERVICE_GET_INTERNET_SUBSCRIPTION_ENDPOINT,
        getMiniPackSubscriptionEndpoint: process.env.SUBSCRIPTION_SERVICE_GET_MINIPACK_SUBSCRIPTION_ENDPOINT,
        activateMiniPackEndpoint: process.env.SUBSCRIPTION_SERVICE_ACTIVATE_MINIPACK_ENDPOINT,
        activateUpgradeSpeedEndpoint: process.env.SUBSCRIPTION_SERVICE_ACTIVATE_UPGRADE_SPEED_ENDPOINT
    },
    fulfillment:{
        fulfillmentGracePeriod: process.env.FULFILLMENT_GRACE_PERIOD,
    }
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
