const log4js = require('log4js');

class Logger {

  constructor(logger = 'User Support Service', level = 'info') {
    this.logger = log4js.getLogger(logger);
    this.logger.level = level;
  }

  trace(message, ...args) {
    this.logger.trace(message, ...args);
  }

  debug(message, ...args) {
    this.logger.debug(message, ...args);
  }

  info(message, ...args) {
    this.logger.info(message, ...args);
  }

  error(message, ...args) {
    this.logger.error(message, ...args);
  }

  warn(message, ...args) {
    this.logger.warn(message, ...args);
  }

  fatal(message, ...args) {
    this.logger.fatal(message, ...args);
  }

}

module.exports = Logger;