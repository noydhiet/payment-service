/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/20/2020
 */
module.exports = {
    POINT_KEY: '36ef7363259eb59056029361d99361092c576613',
    POINT_SOURCE: 'myindihome',
    LIST_VOUCHER_OP: 'ListNewVoucher',
    REWARDS_TYPES: [
        'AUTOMOTIVE',
        'E-COMMERCE',
        'EDUKASI',
        'ENTERTAINMENT',
        'FASHION',
        'FOOD AND BEVERAGE',
        'HEALTH AND BEAUTY',
        'HOME & APPLIANCE',
        'PRIZE',
        'SHOP',
        'SPORT',
        'TELKOM PRODUCT',
        'TRAVEL'
    ],
    REWARDS_ACTIVE_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive'
    },
};
