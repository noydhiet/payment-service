/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/10/2020
 */
module.exports = {
    HTTP_STATUS: {
        SUCCESS: 'success',
        FAIL: 'fail'
    },
    LANGUAGE: {
        EN: 'en',
        ID: 'id'
    },
    LANGUAGE_HEADER: 'Accept-Language',
    DATE_FORMATS: {
        ISO_DATE_FORMAT: 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]',
        PLAIN_DATETIME_FORMAT: 'YYYY-MM-DD HH:mm:ss',
        PLAIN_DATETIME_REVERSE_FORMAT: 'DD-MM-YYYY HH:mm:ss',
        PLAIN_DATETIME_ZONE_FORMAT: 'YYYY-MM-DD HH:mm:ss.S',
        PLAIN_DATE_FORMAT: 'YYYY-MM-DD',
    }
};
