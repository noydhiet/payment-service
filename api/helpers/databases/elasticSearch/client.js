const es = require('elasticsearch');
const esClient = new es.Client({
    host: process.env.ELASTICSEARCH_HOST,
    log: 'trace'
});

module.exports = esClient;
