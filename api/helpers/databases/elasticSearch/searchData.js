const esClient = require('./client');

const searchDoc = async function(indexName, payload, mappingType = null){
    if(mappingType){
        return await esClient.search({
            index: indexName,
            type: mappingType,
            body: payload
        });
    }
    else{
        return await esClient.search({
            index: indexName,
            body: payload
        });
    }
};


module.exports = searchDoc;


async function test(){
    const body = {
        query: {
            match: {
                'title': 'Learn'
            }
        }
    };
    try {
        const resp = await searchDoc('blog', body, 'ciphertrick');
        // eslint-disable-next-line no-console
        console.log(resp);
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
    }
}


//test();
