const esClient = require('./client');


const updateDoc = async function(indexName, id, data, mappingType = null){
    if(mappingType) {
        return await esClient.index({
            index: indexName,
            type: mappingType,
            id: id,
            body: data
        });
    }
    else{
        return await esClient.index({
            index: indexName,
            id: id,
            body: data
        });
    }
};


module.exports = updateDoc;
