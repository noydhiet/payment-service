const esClient = require('./client');
const addmappingToIndex = async function(indexName, mappingType, mapping){
    return await esClient.indices.putMapping({
        index: indexName,
        type: mappingType,
        body: mapping
    });
};

module.exports = addmappingToIndex;


// test function to explain how to invoke.
async function InitialNotification(){
    const mapping = {
        properties: {
            header: {
                type: 'text'
            },
            isChecked: {
                type: 'boolean'
            },
            checkDate: {
                type: 'date'
            },
            referenceId: {
                type: 'text'
            },
            messageType: {
                type: 'text'
            },
            imageUrl: {
                type: 'text'
            },
            timestamp: {
                type: 'date'
            }
        }
    };
    try {
        const resp = await addmappingToIndex('inboxNotifications', 'messages', mapping);
        // eslint-disable-next-line no-console
        console.log(resp);
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
    }
}


//test();
