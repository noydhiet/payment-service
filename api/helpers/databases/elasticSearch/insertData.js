const esClient = require('./client');


const insertDoc = async function(indexName, mappingType = '_doc', data, id){
    return await esClient.index({
        index: indexName,
        id: id,
        body: data
    });
};

const insertBulkDoc = async function(bulkBody){
    return await esClient.bulk({
        body: bulkBody
    });
};


module.exports = {insertDoc,insertBulkDoc};

async function test(){
    let newNotifications = [];
    const metaData = { 'index' : { '_index' : 'payment_records'} };
    const data = {
        invoice: 'MIHX2020130141',
        indiHomeNum: '122207444179',
        amount: '1000',
        custId: '50012454',
        custMsisdn: '1234567890',
        custEmail: 'pccw@dev.com',
        custName: 'pccw user',
        paymentType: 'cc',
        type: 'DEPOSIT',
        status: 'PENDING'
    };
    newNotifications.push(metaData);
    newNotifications.push(data);

    try {
        // eslint-disable-next-line no-console
        console.log('gonna save',newNotifications);
        const resp = await insertBulkDoc(newNotifications);
        // eslint-disable-next-line no-console
        console.log(resp);
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
    }
}


//test();




