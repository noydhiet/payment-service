const esClient = require('./client');
const Logger = require('../../utils/logger');
const logger = new Logger('paymentService');


const createIndex = async function(indexName){
    return await esClient.indices.create({
        index: indexName
    });
};

module.exports = createIndex;


async function test(){
    try {
        const resp = await createIndex('inboxNotifications');
        logger.info(`create index success: ${JSON.stringify(resp)}`);

    } catch (e) {
        logger.error(`create index error. Date: ${new Date()}. Response: ${JSON.stringify(e)}`);
    }
}

//test();
