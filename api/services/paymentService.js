/* eslint-disable no-empty */
/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */

const _ = require('lodash');
const util = require('util');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('paymentService');
const config = require('../config');
const {InternalServerError,BadRequestError} = require('../helpers/error');
const rp = require('request-promise');
var crypto = require('crypto');
const AuthenticationService = require('../middleware/telkom_jwt');
const authenticationService = new AuthenticationService();
const userProfileService = require('../services/userProfileService');
const notificationService = require('./notificationService');
const finpayReturnEndpoint = config.get('/paymentService').getFinpayReturnEndpoint;
const finpaySuccessEndpoint = config.get('/paymentService').getFinpaySuccessEndpoint;
const finpayFaliureEndpoint = config.get('/paymentService').getFinpayFaliureEndpoint;
const tmoneyReturnEndpoint = config.get('/paymentService').getDepositEndpoint;

const getEsPaymentIndex = config.get('/paymentService').getEsPaymentIndex;
const getEsRefundIndex = config.get('/paymentService').getEsRefundIndex;
const getEsPaymenttype = config.get('/paymentService').getEsPaymenttype;
const getEsRefundtype = config.get('/paymentService').getEsRefundType;
const getEsPaymentPackageDetails = config.get('/paymentService').getEsPaymentPackageDetails;

const insertDoc = require('../helpers/databases/elasticSearch/insertData');
const searchDoc = require('../helpers/databases/elasticSearch/searchData');
const updateDoc = require('../helpers/databases/elasticSearch/updateData');

const ssoService = require('./ssoService');
const subscriptionService = require('./subscriptionService');

const moment = require('moment');

const {
    FINPAY_MERCHANT_ID, FINPAY_MERCHANT_PW, FINPAY_ADD_INFO2, FINPAY_NO_SC,
    FINPAY_TIMEOUT, FINPAY_SOF_PAY, FINPAY_ADD_INFO1, MD_TYPE, MD_SIGNATURE,
    BILLCC_CHANNEL, BILL_CC_SHAREDKEY, POSTPAID_CHANNEL, POSTPAID_SHAREDKEY,
    POST_PAID_BILL_SUCCESS_MESSAGE, POST_PAID_SUCCESS_MESSAGE_TYPE,
    PAYMENT_REFUND_NOTIFICATION_HEADER, NOTIFICATION_TYPES, NOTIFICATION_CATEGORIES,
    PRE_PAID_BILL_SUCCESS_MESSAGE, PRE_PAID_SUCCESS_MESSAGE_TYPE, TMONEY_MERCHANT_ID, TMONEY_CHANNEL,
    PREPAID_BILLING_TYPES, DEPOSIT_BILLING_TYPES, NOTIFICATION_TOPICS,NOTIFICATION_GROUPS
} = require('../helpers/utils/constants/paymentConstants');

const paymentOptionMap = new Map([
    ['permata', 'T01'], ['finpay', 'finpay021'], ['mandiri', 'vamandiri'], ['bca', 'vabca'], ['bri', 'briva'],
    ['bni', 'T06'], ['creditdebit', 'cc'], ['linkaja', 'tcash'], ['vabni', 'vabni']
]);

const fulfillmentGracePeriod = parseInt(config.get('/fulfillment').fulfillmentGracePeriod);

module.exports.finpayPayment = async (depositData, userData) => {

    const billDetails = await getBillDetails(depositData.paymentCode);
    if (_.isEmpty(billDetails)) {
        return {
            ok: false,
            status: 404,
            message: 'invalid payment Code',
            data: {}
        };
    }
    const billingType = billDetails._source.billingType;
    const transactionId = billDetails._source.transactionId;

    const jwt = await authenticationService.getJWTFForUser();

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`received user profile. Profile data: ${JSON.stringify(profile)}`);
    const account = profile.accounts.find(ac => ac.indiHomeNum === depositData.indiHomeNum);

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('No matching account found in profile');
    }

    const tMoneyUser = await getTmoneyToken(profile.email);

    //TODO get packet details
    let invoiceNum = depositData.paymentCode;
    let referenceNo = depositData.paymentCode;
    let finpayItems = '[["INDIHOME", amount, 1]]'.replace('amount', depositData.amount.toString());

    // remove this from addon information
    let tmoneyItems = '{"item":[{"itemid":1,"type":"IHQUOTA","nd":"122202220057","namaPaket":"5000","durasi":"","harga":"5000","ppn":500}]}';

    //  define the tmoneyItems based on the billingType
    if (billingType === PREPAID_BILLING_TYPES.SOD) {
        const sodSubscription = await subscriptionService.getSoDSubscription(transactionId);
        if (_.isEmpty(sodSubscription)) {
            return {
                ok: false,
                status: 408,
                message: 'getting the subscription information failed',
                data: {}
            };
        }

        //can check if the userData.userId vs sodSubscription.userId matches as well
        logger.info(`got subscription data as : ${JSON.stringify(sodSubscription)}`);

        // validations for the subscription values
        let indiHomeNum = sodSubscription.indiHomeNum;
        let selectedPackageId = sodSubscription.package.packageId;
        let packageDuration = sodSubscription.package.duration;
        if (_.isEmpty(indiHomeNum) || _.isEmpty(selectedPackageId))
        {
            return {
                ok: false,
                status: 408,
                message: 'invalid subscription information found',
                data: {package: sodSubscription}
            };
        }
        tmoneyItems = `{"item":[{"itemid":1,"type":"IHQUOTA","nd":"${indiHomeNum}","namaPaket":"${selectedPackageId}"\
            ,"durasi":"${packageDuration}","harga":"${depositData.amount}","ppn":0}]}`;

    } else if(billingType === PREPAID_BILLING_TYPES.MINI_PACKS) {
        const minipackSubscription = await subscriptionService.getMiniPackSubscription(transactionId);
        if (_.isEmpty(minipackSubscription)) {
            logger.error('Error in get minipack subscription.', minipackSubscription);
            return {
                ok: false,
                status: 408,
                message: 'getting the subscription information failed',
                data: {}
            };
        }

        logger.info(`got subscription data as : ${JSON.stringify(minipackSubscription)}`);

        // validations for the subscription values
        let indiHomeNum = minipackSubscription.indiHomeNum;
        let selectedPackageId = minipackSubscription.minipackId;
        let prepaidPrice = minipackSubscription.minipack.prepaidPrice;

        if (_.isEmpty(indiHomeNum) || _.isEmpty(selectedPackageId) || depositData.amount.toString() !== prepaidPrice.toString())
        {
            return {
                ok: false,
                status: 408,
                message: 'invalid subscription information found',
                data: {package: minipackSubscription}
            };
        }
        tmoneyItems = `{"item":[{"itemid":1,"type":"IHQUOTA","nd":"${indiHomeNum}","namaPaket":"${selectedPackageId
        }","durasi":"","harga":"${depositData.amount}","ppn":0}]}`;
    }

    let paymentType = paymentOptionMap.get(depositData.paymentType.toLowerCase());

    let body = {};
    let url = process.env.FINPAY_URL;
    let type = 'finpay';

    if (depositData.paymentType === 'saldo') {
        body = {
            idTmoney: tMoneyUser.idTmoney,
            idFusion: tMoneyUser.idFusion,
            token: tMoneyUser.token,
            destAccount: account.email,
            amount: depositData.amount,
            pin: depositData.pin
        };
        url = process.env.TMOENY_PREPAID_URL;
        type = 'saldo';

    }else{

        body = {
            merchant_id: FINPAY_MERCHANT_ID,
            merchant_pass: FINPAY_MERCHANT_PW,
            invoice: invoiceNum,
            amount: depositData.amount,
            cust_id: account.ncli,
            cust_msisdn: profile.primaryPhone,
            cust_email: account.email,
            cust_name: profile.name,
            return_url: finpayReturnEndpoint,
            success_url: finpaySuccessEndpoint+'?transactionId='+referenceNo+'&amount='+depositData.amount,
            failed_url: finpayFaliureEndpoint,
            items: finpayItems,
            timeout: FINPAY_TIMEOUT,
            sof_id: paymentType,
            sof_type: FINPAY_SOF_PAY,
            add_info1: FINPAY_ADD_INFO1,
            add_info2: FINPAY_ADD_INFO2,
            customer_nik: depositData.indiHomeNum,
            no_sc: FINPAY_NO_SC
        };
    }

    const paymentRecord = {
        referenceNo:referenceNo,
        invoice:invoiceNum,
        indiHomeNum:depositData.indiHomeNum,
        fixedPhoneNum:account.fixedPhoneNum,
        internetNum:account.internetNum,
        amount: depositData.amount,
        custMsisdn: profile.primaryPhone,
        custEmail: account.email,
        custName: profile.name,
        cust_ncli: account.ncli,
        paymentType:paymentType,
        type:depositData.type,
        paymentDate:new Date(),
        paymentCode:referenceNo,
        ticketId:'',
        status:'PENDING'
    };

    const paymentResponse = await payment(jwt, body, type, url);

    if(paymentResponse.ok){
        if(paymentResponse.data.paymentCode){
            paymentRecord.paymentCode = paymentResponse.data.paymentCode;
        }
        if(paymentResponse.data.ticketId){
            paymentRecord.ticketId = paymentResponse.data.ticketId;
        }
        const savePaymentStatus = await savePaymentDetails(getEsPaymentIndex,paymentRecord);
        if(savePaymentStatus.ok){
            return paymentResponse;
        }
        return {
            ok:false,
            status:406,
            message:'payment service temporaly down',
            data: {}
        };
    }

    return {
        ok:false,
        status:paymentResponse.status,
        message:paymentResponse.message,
        data: {}
    };

};



// generic method to handle payment apis.

async function payment(jwt, payload, type, url) {
    let pin = '';
    if(payload.pin){
        pin = payload.pin; //clear out the pin for initial request
        payload.pin = '';
    }

    let options = {
        method: 'POST',
        url: url,
        headers:
      {
          authorization: `Bearer ${jwt}`,
          'content-type': 'application/json'
      },
        body:payload,
        json: true,
        strictSSL: false
    };

    let resp = await rp.post(options).catch(err => {
        logger.error('Error connecting to' + type +`server. Error: ${JSON.stringify(err)}`);
        return {
            ok:false,
            status:500,
            message:'Error connecting to'  + type + 'server',
            data: {}
        };
    });
    logger.info(`got response for Request: ${JSON.stringify(options)}, \t Response: ${JSON.stringify(resp)}`);
    if (!util.isNullOrUndefined(resp)) {

        if(resp.statusCode === '0'){

            if(type === 'finpay'){
                return {
                    ok:true,
                    status:200,
                    message:'payment gateway request success',
                    data: {
                        paymentCode:resp.data.payment_code,
                        redirectUrl:resp.data.redirect_url
                    }
                };
            }
            else if(type === 'billCC'){
                return {
                    ok:true,
                    status:200,
                    message:'payment gateway request success',
                    data: {
                        redirectUrl:resp.data.returnUrl
                    }
                };
            }
            else if(type === 'linkaja'){
                return {
                    ok:true,
                    status:200,
                    message:'payment gateway request success',
                    data: {
                        redirectUrl:resp.data.urlTCash
                    }
                };
            }
            else if(type === 'saldo'){
                payload.transactionType = '2';
                payload.pin = pin;
                payload.transactionID = resp.data.transactionID;
                payload.refNo = resp.data.refNo;
                resp = await tMoneyApiPostRequest(config.get('/tMoney').tMoneyPostPaidEndpoint, payload);
                if(resp.statusCode === '-2' || _.isEmpty(resp)){
                    logger.error('TMoney Postpaid Payment Failed.');
                    return {ok:false ,status:409, message:'TMoney Postpaid payment failed'+ resp.returnMessge, data:{}};
                }
                return {ok:true, status:2004, message:'TMoney Payment success.', data: resp.data}
            }
        }
        if(resp.statusCode === '-2'){
            logger.error(`Error core api. Error: ${JSON.stringify(resp)}`);
            return {
                ok:false,
                status:409,
                message:resp.returnMessage,
                data: { }
            };
        }

        logger.error('Error with' + type + `server. Error: ${JSON.stringify(resp)}`);
        return {
            ok:false,
            status:500,
            message:'Error connecting to finpay server',
            data: {}
        };

    }

}

async function getdateFormatted() {
    let dateobj= new Date() ;
    let dd   = ('00'+dateobj.getDate().toString()).slice(-2);
    let mm   = ('00'+(dateobj.getMonth()+1).toString()).slice(-2); //January is 0!
    let yyyy = dateobj.getFullYear().toString();
    let hour = ('00'+dateobj.getHours().toString()).slice(-2);
    let minu = ('00'+dateobj.getMinutes().toString()).slice(-2);
    let sec = ('00'+dateobj.getSeconds().toString()).slice(-2);
    return yyyy+mm+dd+hour+minu+sec;
}

// TO save payment details
async function savePaymentDetails(getEsPaymentIndex, paymentData){
    try {
        let id = Date.now() + Math.floor(Math.random() * (100) + 100);
        const resp = await insertDoc.insertDoc(getEsPaymentIndex,'_doc',paymentData,id);
        logger.info(`Success saving payment data. Success: ${JSON.stringify(resp)}`);
        return {ok: true, status: 200, message: 'Success saving payment data', data: resp};

    } catch (e) {
        logger.error(`Error saving payment data. Error: ${JSON.stringify(e)}`);
        return {ok: false, status: 500, message: 'Error saving data', data: {}};
    }
}

/**
 *
 * @param billDetails
 * @returns {Promise<*>}
 */
async function sendNewInstallationAvailableNotification(billDetails) {

    const transactionId = billDetails._source.transactionId;
    const selectedPackage = await subscriptionService.getSelectedInternetPackage(transactionId);
    if (_.isEmpty(selectedPackage)) {
        throw new InternalServerError('package is not found');
    }
    const userId = selectedPackage.userId;
    const installationDeadline = new Date();
    installationDeadline.setHours(installationDeadline.getHours() + fulfillmentGracePeriod);

    const appointment = {
        userId,
        transactionId,
        installationDeadline,
        package: selectedPackage
    };
    //todo: close the previous activity card: continue to deposit for this user
    const notification = {
        notifications: [
            {
                userId,
                header: `Schedule an Installation before ${moment(installationDeadline).format('DD MMMM YYYY')}`,
                isChecked: false,
                referenceId: transactionId,
                notificationType: NOTIFICATION_TYPES.ACTIVITY,
                category: NOTIFICATION_CATEGORIES.NEW_INSTALLATION,
                imageUrl: '',
                group: NOTIFICATION_GROUPS.FULFILLMENT,
                body: {
                    idHeader: 'Order ID',
                    transactionId: transactionId,
                    indiHomeNum: selectedPackage.indiHomeNum,
                    topic: NOTIFICATION_TOPICS.NEW_INSTALLATION,
                    description: `Schedule an Installation before ${moment(installationDeadline).format('DD MMMM YYYY')}`,
                    data: {
                        appointment,
                        installationDeadline,
                    }
                }
            }
        ]
    };
    return await notificationService.sendHomeNotification(notification, userId)
        .catch(e => {
            logger.error('send notification failed', e)
        });
}

// Used for api callback to update payment status.
module.exports.updatePaymentStatus = async (type,invoice, paymentStatus) => {

    let body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'match': {
                            'invoice':invoice
                        }
                    }
                ]
            }
        }
    };


    if(type === 'TMONEY'){
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'match': {
                                'ticketId':invoice
                            }
                        }
                    ]
                }
            }
        };
    }

    try {
        const resp = await searchDoc(getEsPaymentIndex, body, getEsPaymenttype);
        let result = resp['hits']['hits'];

        if (result.length === 0) {
            logger.info('No rusults found for the reference number.');
            return {ok: true, status: 404, message: 'No rusults found for the reference number.', data: {}};
        }

        let bodyIn = result[0];
        if(bodyIn !== null){
            bodyIn._source.status = paymentStatus.toUpperCase();
            try{
                const resp2 = await updateDoc(getEsPaymentIndex,bodyIn._id,bodyIn._source, getEsPaymenttype);

                logger.info(`Success updating payment data. Success: ${JSON.stringify(resp2)}`);
                if(paymentStatus.toUpperCase() === 'SUCCESS'){
                    if (bodyIn._source.type === 'DEPOSIT') {

                        const billDetails = await getBillDetails(bodyIn._source.referenceNo);
                        if (_.isEmpty(billDetails) || _.isEmpty(billDetails._source.billingType)) {
                            logger.error('Error invalid billing information', billDetails);
                            return {
                                ok: false,
                                status: 400,
                                message: 'Error: invalid billing information',
                                data: {}
                            };
                        }

                        switch (billDetails._source.billingType) {
                        case DEPOSIT_BILLING_TYPES.FULFILLMENT: {
                            //send user notification of new installation is available
                            logger.info(`billing type : ${DEPOSIT_BILLING_TYPES.FULFILLMENT} found.`);
                            const notification = await sendNewInstallationAvailableNotification(billDetails);
                            if (_.isEmpty(notification) || _.isEmpty(notification.data)) {
                                return {ok: true, status: 500, message: 'Error in sending notification.', data: {}};
                            }
                            logger.info(`fulfilment notification sent: ${JSON.stringify(notification)}`);
                            break;
                        }
                        default: {
                            return {
                                ok: true,
                                status: 400,
                                message: `Invalid billing type: ${billDetails._source.billingType}`,
                                data: {}
                            };
                        }
                        }

                        return {ok: true, status: 200, message: 'Success updating payment data.', data: {}};
                        /*
                        TODO continue with the createInbox process.
                        The reference ID is the ID of 'user_billing_cart' index,Through the index
                        get the details Bill or the Package.Use the data to send the needed notification*/


                        // later informed to remove the manageDeposit call from the deposit process.

                        // const depositResponse = await manageDeposit(bodyIn._source);
                        // if(depositResponse.ok){
                        //     return {ok:true,status:200,message:'Success updating payment data.',data:{}};
                        // }
                        // return {ok:true,status:406,message:'Error with Telkom Core manageDepositApi.',data:{
                        //     errorDetails:depositResponse.message
                        // }};
                    }
                    if (bodyIn._source.type === 'POSTPAID') {

                        const paymentDetails = await getBillDetails(bodyIn._source.referenceNo);
                        if (paymentDetails) {
                            let bill = paymentDetails._source.packageData[0];
                            let inboxNotification = {
                                messages: [
                                    {
                                        header: POST_PAID_BILL_SUCCESS_MESSAGE,
                                        isChecked: false,
                                        referenceId: bill.code,
                                        messageType:'personal',
                                        messageSubType: POST_PAID_SUCCESS_MESSAGE_TYPE,
                                        imageUrl: '',
                                        userId: bodyIn._source.userId
                                    }
                                ]
                            };

                            notificationService.sendUserNotification(inboxNotification,bodyIn._source.userId);
                            return {ok:true,status:200,message:'Success updating payment data.',data:{}};
                        }

                        return {ok:true,status:404,message:'Error wrong reference number.',data:{}};

                    }

                    if (bodyIn._source.type === 'PREPAID') {

                        //TODO continue with the prepaid flow

                        const paymentDetails = await getBillDetails(bodyIn._source.referenceNo);
                        if (paymentDetails) {
                            let bill = paymentDetails._source.packageData[0];
                            const billingType = paymentDetails._source.billingType;
                            if (_.isEmpty(billingType)) {
                                return {
                                    ok: false,
                                    status: 400,
                                    message: 'Error invalid billing type',
                                    data: {}
                                };
                            }
                            switch (billingType) {
                                case PREPAID_BILLING_TYPES.SOD : {
                                    logger.info(`prepaid payment successful for ${PREPAID_BILLING_TYPES.SOD} `);
                                    const activated = await subscriptionService.activateSoDPackage(paymentDetails._source.transactionId);
                                    logger.info(`product subscription activated SoD response: ${activated}`);
                                    if (_.isEmpty(activated)) {
                                        return {ok: false, status: 409, message: 'speed on demand activation failed in' +
                                                ' core system', data: {}};
                                    }
                                    break;
                                }
                                case PREPAID_BILLING_TYPES.UPGRADE_SPEED :{
                                    logger.info(`prepaid payment successful for ${PREPAID_BILLING_TYPES.UPGRADE_SPEED} `);
                                    const activated = await subscriptionService.activateUpgradeSpeedPackage(paymentDetails._source.transactionId);
                                    logger.info(`product subscription activated upgrade speed response: ${activated}`);
                                    if (_.isEmpty(activated)) {
                                        return {ok: false, status: 409, message: 'upgrade speed activation failed in' +
                                                ' core system', data: {}};
                                    }
                                    break;
                                }
                                case PREPAID_BILLING_TYPES.MINI_PACKS: {
                                    logger.info(`prepaid payment successful for ${PREPAID_BILLING_TYPES.MINI_PACKS}.`);
                                    const activated = await subscriptionService.activateMiniPackPackage(paymentDetails._source.transactionId);
                                    logger.info(`product subscription activated response: ${JSON.stringify(activated)}`);
                                    if (_.isEmpty(activated)) {
                                        return {
                                            ok: false,
                                            status: 409,
                                            message: 'minipack activation failed in core system', data: {}
                                        };
                                    }
                                    break;
                                }
                                //TODO: handle other success scenarios for other billingTypes
                                default: {
                                    break;
                                }
                            }
                            let inboxNotification = {
                                messages: [
                                    {
                                        header: PRE_PAID_BILL_SUCCESS_MESSAGE,
                                        isChecked: false,
                                        referenceId: bill.code,
                                        messageType:'personal',
                                        messageSubType: PRE_PAID_SUCCESS_MESSAGE_TYPE,
                                        imageUrl: '',
                                        userId: bodyIn._source.userId
                                    }
                                ]
                            };

                            notificationService.sendUserNotification(inboxNotification, bodyIn._source.userId)
                                .then(() => logger.info('payment notification sent successfully'));

                            return {ok: true, status: 200, message: 'Success updating payment data.', data: {}};
                        }

                        return {ok: false, status: 404, message: 'Error wrong reference number.', data: {}};
                    }

                }

                if(paymentStatus.toUpperCase() === 'FAILURE'){
                    /*
                        TODO Call the subscription services and notifiy the the process failure.
                        The reference ID is the ID of 'user_billing_cart' index,Through the index
                        get the details Bill or the Package.Use the data to send the needed notification*/
                    return {ok:true,status:200,message:'Success updating payment data.',data:{}};

                }

            }catch(e){
                logger.error(`Error updating payment data. Error: ${JSON.stringify(e)}`);
                return {ok:false,status:406,message:'Error updating payment data. Error.',data:{}};
            }
        }
    } catch (e) {
        logger.error(`Error fetching payment data. Error: ${JSON.stringify(e)}`);
        return {ok:false,status:500,message:'Error fetching payment data.',data:{}};
    }
};



// This is the second step of the Deposit flow.

async function manageDeposit(payloadIn) {
    const jwt = await authenticationService.getJWTFForUser();

    let transdateTime = await getdateFormatted();

    let payload = {
        request_type:MD_TYPE,
        trans_number: 'MIHX'+ transdateTime,
        trans_datetime: transdateTime,
        amount: payloadIn.amount,
        trans_desc: 'MIHX'+ transdateTime,
        ncli: payloadIn.cust_ncli,
        no_sc: FINPAY_NO_SC,
        invoice: payloadIn.invoice,
        no_tlp: payloadIn.fixedPhoneNum,
        no_inet: payloadIn.internetNum,
        acc_bank_no: '',
        acc_bank_name: '',
        acc_bank_code: '',
        customer_name: payloadIn.custName,
        payment_code: payloadIn.paymentCode
    };

    let signaturePayload = {
        requestType: 'createDeposit',// payload.request_type,
        transNumber: '10070286090511', //payload.trans_number,
        reqDtime: transdateTime, //payload.trans_datetime, 20200304101400
        transAmount: '500', // payload.amount,
        transDesc: '10138591470001',// payload.trans_desc,
        noCLI: '1',// payload.ncli,
        noSC: '10070286090021',// payload.no_sc,
        noInvoice: '141149102416',// payload.invoice,
        noTlp:  '0217199608',//payload.no_tlp,
        noInet: '122604200712',//payload.no_inet,
        accBankNo: '335133361',//payload.acc_bank_no,
        accBankName: 'Muhammad Abdul Jafar Sidiq',//payload.acc_bank_name,
        accBankCode: '009', //payload.acc_bank_code,
        custName: 'fadli',//payload.customer_name,
        paymentCode: '021113029374',//payload.payment_code
    };

    let joinedStr = '';
    Object.keys(signaturePayload).sort().forEach(function(key) {
        joinedStr += signaturePayload[key];
    });

    var hmac = crypto.createHmac('sha256', '6A4F234552516F3F5E54714E');
    let data = hmac.update(joinedStr);
    let signature = data.digest('hex').toUpperCase();

    signaturePayload['signature'] = signature;

    let options = {
        method: 'POST',
        url: process.env.MANAGE_DEPOSIT_URL,
        headers:
      {
          authorization: 'Bearer ' + jwt,
          'content-type': 'application/json'
      },
        body:signaturePayload,
        json: true,
        strictSSL: false
    };

    const resp = await rp.post(options).catch(err => {
        logger.error(`Error connecting to core api server. Error: ${JSON.stringify(err)}`);
        return {
            ok:false,
            status:500,
            message:'Error connecting to core api ' + process.env.MANAGE_DEPOSIT_URL,
            data: {}
        };
    });
    if (!util.isNullOrUndefined(resp)) {

        if(resp.statusCode === '0'){

            return {
                ok:true,
                status:200,
                message:'deposit success',
                data: {}
            };
        }
        if(resp.statusCode === -2){
            logger.error(`Error core api. Error: ${JSON.stringify(resp)}`);
            return {
                ok:false,
                status:404,
                message:resp.returnMessage,
                data: { }
            };
        }
        logger.error(`Error with core api server. Error: ${JSON.stringify(resp)}`);
        return {
            ok:false,
            status:500,
            message:'Error connecting to core api ' + process.env.MANAGE_DEPOSIT_URL,
            data: {}
        };
    }
}


module.exports.postpaidPayment = async (paymentData, userData) => {
    const jwt = await authenticationService.getJWTFForUser();
    let transdateTime = await getdateFormatted();

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`received user profile. Profile data: ${JSON.stringify(profile)}`);
    const account = profile.accounts.find(ac => ac.indiHomeNum === paymentData.indiHomeNum);

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('No matching account found in profile');
    }

    let invoiceNum = 'MIHX'+ paymentData.paymentCode;
    let referenceNo = paymentData.paymentCode;
    let requestBody ={};
    let paymentType ='';
    let paymentUrl ='';
    let userId = userData.userId;

    const billCCBody = {
        transIdMerchant: invoiceNum,
        noTel:account.fixedPhoneNum,
        channelSource: BILLCC_CHANNEL,
        sharedKey: BILL_CC_SHAREDKEY,
        requestDateTime:transdateTime,
        email:account.email,
        handphone: profile.primaryPhone,
        requesterName: profile.name,
        urlSuccess:finpaySuccessEndpoint+'?transactionId='+referenceNo+'&amount='+paymentData.amount,
        urlFailed:finpayFaliureEndpoint,
        urlCallback: finpayReturnEndpoint,
    };

    const postpaidBillBody = {
        invoiceNo: invoiceNum,
        serviceNo:paymentData.indiHomeNum,
        email:  account.email,
        msisdn: profile.primaryPhone,
        name: profile.name,
        sharedKey:POSTPAID_SHAREDKEY,
        channelSource:POSTPAID_CHANNEL,
        redirectUrl:finpayReturnEndpoint,
    };

    const paymentRecord = {
        referenceNo:referenceNo,
        invoice:invoiceNum,
        indiHomeNum:paymentData.indiHomeNum,
        fixedPhoneNum:account.fixedPhoneNum,
        internetNum:account.internetNum,
        amount: paymentData.amount,
        custMsisdn: profile.primaryPhone,
        custEmail: account.email,
        custName: profile.name,
        cust_ncli: account.ncli,
        userId:userId,
        paymentType: paymentData.paymentType,
        type:paymentData.type,
        paymentDate:new Date(),
        paymentCode:'',
        status:'PENDING'
    };


    if (paymentData.paymentType.toLowerCase() === 'linkaja') {
        requestBody = postpaidBillBody;
        paymentType = 'linkaja';
        paymentUrl = process.env.POSTPAID_URL;

    }
    else if(paymentData.paymentType.toLowerCase() == 'creditdebit'){
        requestBody = billCCBody;
        paymentType = 'billCC';
        paymentUrl = process.env.BILL_CC_URL;
    }
    else if(paymentData.paymentType.toLowerCase() == 'saldo'){
        const userData = await ssoService.getUserSSOUserData(userId);
        if (!userData) {
            throw new BadRequestError('Invalid userId');
        }

        const tMoneyUser = await getTmoneyToken(userData.data.email);
        if(util.isNullOrUndefined(tMoneyUser)){
            return {ok:true, status:500, message:'Account not found in core system.', data:{}};
        }

        requestBody = {
            transactionType: "1",
            idTmoney: tMoneyUser.idTmoney,
            idFusion: tMoneyUser.idFusion,
            token: tMoneyUser.token,
            productCode: "TMONEYTLKM",
            amount: paymentData.amount,
            billNumber: paymentData.indiHomeNum,
            pin: paymentData.pin,
            transactionID: "",
            refNo: ""
        };
        paymentType = 'saldo';
        paymentUrl=process.env.TMONEY_POSTPAID_URL;
    }

    const paymentResponse = await payment(jwt, requestBody, paymentType, paymentUrl);

    if(paymentResponse.ok){
        if(paymentResponse.data.paymentCode){
            paymentRecord.paymentCode = paymentResponse.data.paymentCode;
        }
        if(paymentData.paymentType.toLowerCase() == 'saldo'){
            paymentRecord.status = 'SUCCESS';
        }
        const savePaymentStatus = await savePaymentDetails(getEsPaymentIndex, paymentRecord);
        if(savePaymentStatus.ok){
            return paymentResponse;
        }
        return {
            ok:false,
            status:406,
            message:'payment service temporaly down',
            data: {}
        };
    }

    return {
        ok:false,
        status:paymentResponse.status,
        message:paymentResponse.message,
        data: {}
    };

};


module.exports.generatePaymentSession = async (paymentData,userDataIn) => {

    const userData = await ssoService.getUserSSOUserData(userDataIn.userId);
    if (!userData) {
      throw new BadRequestError('Invalid userId');
    }
    let userEmail = userData.data.email;

    const paymentRecord = {
        custEmail: userEmail,
        indiHomeNum: paymentData.indiHomeNum,
        paymentDate: new Date(),
        billingType: paymentData.billingType,
        packageData: paymentData.items,
        transactionId: paymentData.transactionId,
    };

    const savePaymentStatus = await savePaymentDetails(getEsPaymentPackageDetails,paymentRecord);
    if(savePaymentStatus.ok){
        let paymentCode = savePaymentStatus.data._id;
        return {ok: true, status: 200, message: 'payment code created', data: {paymentCode: paymentCode}};
    }
    return {
        ok:false,
        status:406,
        message:'payment service temporaly down',
        data: {}
    };
};




async function getBillDetails(referenceId) {

    const body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'match': {
                            '_id':referenceId
                        }
                    }
                ]
            }
        }
    };
    try {
        const resp = await searchDoc(getEsPaymentPackageDetails,body);
        let result = resp['hits']['hits'];

        if (result.length === 0) {
            logger.info('No results found for the reference number.');
            return null;
        }
        return result[0];
    } catch (e) {
        logger.error('error in es: connection', e);
        return null;
    }
}

/**
 * Get completed deposit information
 * @param indiHomeNum
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.getDepositInfo = async (indiHomeNum) => {
    const body = {
        'query': {
            'bool': {
                'must' : [
                    {
                        'match' : {
                            'indiHomeNum' : indiHomeNum
                        }
                    },
                    {
                        'match' : {
                            'status' : 'SUCCESS'
                        }
                    },
                    {
                        'match' : {
                            'type' : 'DEPOSIT'
                        }
                    }
                ]
            }
        }
    };
    try {
        const resp = await searchDoc(getEsPaymentIndex,body, getEsPaymenttype);
        let result = resp['hits']['hits'];

        if (result.length === 0 || _.isEmpty(result)) {
            logger.error('No results found for the indiHome number.');
            return {ok: true, status: 404, message: 'No results found for the indiHome number.', data: {}};
        }
        const respData = result.map(el => el._source);
        return {ok:true,status:200,message:'Successfully received deposit payment data.',data: respData};

    }catch(e){
        logger.error('Error fetching deposit payment data.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error fetching payment data.',data:{}};

    }
};

/**
 * Get completed monthly payment info
 * @param {String} indiHomeNum - indiHome number
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.getMonthlyPaymentInfo = async (indiHomeNum) => {
    const body = {
        'query': {
            'bool': {
                'must_not' : [
                    {
                        'match' : {
                            'type' : 'DEPOSIT'
                        }
                    }
                ],
                'must' : [
                    {
                        'match' : {
                            'indiHomeNum' : indiHomeNum
                        }
                    },
                    {
                        'match' : {
                            'status' : 'SUCCESS'
                        }
                    }
                ]
            }
        }
    };
    try {
        const resp = await searchDoc(getEsPaymentIndex, body, getEsPaymenttype);
        let result = resp['hits']['hits'];

        if (result.length === 0 || _.isEmpty(result)) {
            logger.error('No results found for the indiHome number.');
            return {ok: true, status: 404, message: 'No results found for the indiHome number.', data: {}};
        }
        const respData = result.map(el => el._source);
        return {ok:true,status:200,message:'Successfully received deposit payment data.',data: respData};

    }catch(e){
        logger.error('Error fetching deposit payment data.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error fetching payment data.',data:{}};

    }
};

async function getTmoneyToken(userEmail){
    const jwt = await authenticationService.getJWTFForUser();
    let date = new Date();
    let dateStr =
        date.getFullYear()  + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getDate()).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2) + ':' +
        ('00' + date.getSeconds()).slice(-2);

    let options = {
        method: 'POST',
        url: config.get('/tMoney').tmoneyLoginEndpoint,
        headers:
            {
                'Authorization': 'Bearer ' + jwt,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        body: {
            'userName': userEmail,
            'password': '',
            'datetime': dateStr
        },
        json: true,
        strictSSL: false
    };

    let resp = await rp(options);
    return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data.user : null;
}

async function tMoneyApiPostRequest(url, payload){

    const jwt = await authenticationService.getJWTFForUser();

    let options = {
        method: 'POST',
        url: url,
        headers:
            {
                'Authorization': 'Bearer ' + jwt,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        body: payload,
        json: true,
        strictSSL: false
    };
    return await rp(options);
}

/**
 * Get completed monthly payment info
 * @param {Object} payload - change pin payload
 * @param {String} userEmail - user email
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.changeTmoneyPin = async (payload, userId) => {

    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }

    const tMoneyUser = await getTmoneyToken(userData.data.email);

    if(util.isNullOrUndefined(tMoneyUser)){
        return {ok:true, status:500, message:'Account not found in core system.', data:{}};
    }
    try {
        let changeTmoneyPayload = {
            idTmoney: tMoneyUser.idTmoney,
            idFusion: tMoneyUser.idFusion,
            token: tMoneyUser.token,
            oldPin: payload.oldPin,
            newPin: payload.newPin
        };
        const resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneyChangePinEndpoint, changeTmoneyPayload);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('Change T-Money wallet pin failed.');
            return {ok:true,status:409,message:'Change T-Money wallet pin failed.',data:{}};
        }
        return {ok:true,status:200,message:'Successfully Changed T-Money wallet pin.',data: resp.data};

    }catch(e){
        logger.error('Error changing T-Money wallet pin.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error changing T-Money wallet pin.',data:{}};
    }
};

/**
 * Get completed monthly payment info
 * @param {String} userId - sso id
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.resetTmoneyPin = async (userId) => {

    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }

    const tMoneyUser = await getTmoneyToken(userData.data.email);

    if(util.isNullOrUndefined(tMoneyUser)){
        return {ok:true,status:500,message:'Account not found in core system.',data:{}};
    }
    try {
        let changeTmoneyPayload = {
            idTmoney: tMoneyUser.idTmoney,
            idFusion: tMoneyUser.idFusion,
            token: tMoneyUser.token
        };
        const resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneyResetPinEndpoint, changeTmoneyPayload);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('Reset T-Money wallet pin failed.');
            return {ok:true,status:409,message:'Reset T-Money wallet pin failed.',data:{}};
        }
        return {ok:true,status:200,message:'Successfully reset T-Money wallet pin.',data: resp.data};

    }catch(e){
        logger.error('Error resetting T-Money wallet pin.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error resetting T-Money wallet pin.',data:{}};
    }
};

/**
 * Check T-money user
 * @param {String} userId - sso id
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.checkTmoneyUserEmail = async (userId) => {

    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }

    try {
        let checkUserEmail = {
            username: userData.data.email
        };
        const resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneyEmailCheck, checkUserEmail);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('check email failed.');
            return {ok:true,status:409,message:'Check email failed.',data:{}};
        }

        if (resp.data.resultCode === '187'){
            return {ok:true,status:200,message:'check email success, user already registered', data: { registrationRequired : 0 }};
        }

        return {ok:true,status:200,message:'check email success, username available.', data: { registrationRequired : 1 }};

    }catch(e){
        logger.error('Error check T-money user.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error check T-money user.',data:{}};
    }
};

/**
 * Check T-money user signup
 * @param {String} userId - sso id
 * @param {Object} payload - new pin and new password
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.tmoneyUserSignUp = async (payload, userId) => {

    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }

    try {
        let signUpUserPayload = {
            accType : '',
            userName: userData.data.email,
            password: payload.password,
            pin: payload.pin,
            fullName: userData.data.name,
            phoneNo: userData.data.mobile
        };
        const resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneySignUp, signUpUserPayload);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('sign up failed.');
            return {ok:true,status:409,message:'sign up failed.',data:{}};
        }
        return {ok:true,status:200,message:'sign up success.',data: resp.data};

    }catch(e){
        logger.error('Error user sign up.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error user sign up.',data:{}};
    }
};

/**
 * transfer from t-money to linkaja
 * @param {Object} payload - change pin payload
 * @param {String} userEmail - user email
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.transferToLinkaja = async (payload, userId) => {

    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }

    const tMoneyUser = await getTmoneyToken(userData.data.email);

    if(util.isNullOrUndefined(tMoneyUser)){
        return {ok:true,status:500,message:'Account not found in core system.',data:{}};
    }
    try {
        let transferToLinkajaPayload= {
            transactionType: '1',
            idTmoney: tMoneyUser.idTmoney,
            idFusion: tMoneyUser.idFusion,
            token: tMoneyUser.token,
            productCode: 'TMONEYTCASH',
            amount: payload.amount,
            billNumber: payload.linkajaMobileNumber,
            pin: '',
            transactionID: '',
            refNo: ''
        };
        const resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneyTansferToLinkaja, transferToLinkajaPayload);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('Transfer to linkaja failed.');
            return {ok:true,status:409,message:'Transfer to linkaja failed.',data:{}};
        }

        transferToLinkajaPayload.transactionType = '2';
        transferToLinkajaPayload.pin = payload.pin;
        transferToLinkajaPayload.transactionID = resp.data.transactionID;
        transferToLinkajaPayload.refNo = resp.data.refNo;
        resp = await tMoneyApiPostRequest(config.get('/tMoney').tmoneyTansferToLinkaja, transferToLinkajaPayload);
        if(resp.statusCode === '-2' || _.isEmpty(resp)){
            logger.error('TMoney Postpaid Payment Failed.');
            return {ok:true,status:409,message:'TMoney Postpaid payment failed'+ resp.returnMessge, data:{}};
        }

        return {ok:true,status:200,message:'Successfully transferred to Linkaja.',data: resp.data};

    }catch(e){
        logger.error('Error in transfer to Linakaja.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error in transfer to Linakaja.',data:{}};
    }
};

/**
 * transfer from t-money to linkaja
 * @param {Object} payload - refund payload
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.refundPayment = async (payload) => {

    const refundTypes = ['AVAILABLE', 'PROCESSING', 'PROCESSED', 'FAILED'];
    if(!refundTypes.includes(payload.status.toUpperCase())){
        logger.error('Error. invalid refund status.');
        throw new BadRequestError('Invalid refund status');
    }

    return await updateRefundStatus(payload);
};

 async function updateRefundStatus(payload){
    let body = {
        'query': {
            'match': {
                'invoice': payload.invoiceNo
            }
        }
    };

    try {
        const resp = await searchDoc(getEsRefundIndex, body);
        let result = resp['hits']['hits'];

        if(result.length == 0){
            return saveRefundDetails({
                bookingId: payload.bookingId,
                invoice: payload.invoiceNo,
                amount: payload.amount,
                type: payload.type,
                reason: payload.reason,
                processedDate: payload.processedDate,
                status: payload.status
            });
        }

        let bodyIn = result[0];
        if(bodyIn !== null){
            bodyIn._source.status = payload.status.toUpperCase();

            try{
                await updateDoc(getEsRefundIndex, bodyIn._id, bodyIn._source);

                const paymentResponse = await searchDoc(getEsPaymentIndex, body);
                let payment = paymentResponse['hits']['hits'];
                if(payment.length == 0){
                    logger.info('No results found for the invoice number.');
                    return {ok:true,status:404,message:'No results found for the invoice number.',data:{}};
                }

                let header = '';
                if(bodyIn._source.status === 'AVAILABLE'){
                    header = 'Your Installation Cancelled, Refund available';
                }
                else if(bodyIn._source.status === 'PROCESSING'){
                    header = 'Refund processing';
                }
                else if(bodyIn._source.status === 'PROCESSED'){
                    header = 'Refund processed';
                }
                else {
                    header = 'Refund failed';
                }

                const notification = {
                    notifications: [
                        {
                            userId: payment[0]._source.userId,
                            header: header,
                            isChecked: false,
                            referenceId: payload.bookingId,
                            messageType: NOTIFICATION_TYPES.ACTIVITY,
                            category: NOTIFICATION_CATEGORIES.PAYMENT_REFUND,
                            imageUrl: '',
                            body: {
                                topic: header,
                                description: header,
                                data: bodyIn._source
                            }
                        }
                    ]
                };

                await notificationService.sendUserNotification(notification, payment[0]._source.userId);
                return {ok:true,status:200,message:'Success updating payment data.',data:{}};
            }
            catch (e) {
                logger.error(`Error updating refund data. Error: ${JSON.stringify(e)}`);
                return {ok:false,status:406,message:'Error updating refund data. Error.',data:{}};
            }
        }

    } catch (e) {
        logger.error(`Error fetching refund data. Error: ${JSON.stringify(e)}`);
        return {ok:false,status:500,message:'Error fetching refund data.',data:{}};
    }
};


async function saveRefundDetails(refundData){
    try {
        const metaData = { 'index' : { '_index' : getEsRefundIndex} };
        let refundRecords = [];
        refundRecords.push(metaData);
        refundRecords.push(refundData);
        const resp = await insertDoc.insertBulkDoc(refundRecords);
        logger.info(`Success saving refund data. Success: ${JSON.stringify(resp)}`);
        return {ok:true,status:200,message:'Success saving refund data',data:resp};

    } catch (e) {
        logger.error(`Error saving refund data. Error: ${JSON.stringify(e)}`);
        return {ok:false,status:500,message:'Error saving data',data:{}};
    }
}


/**
 * transfer from t-money to linkaja
 * @param {Object} payload - refund payload
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.updatePaymentRefund = async (payload) => {

    let body = {
        'query': {
            'match': {
                'invoice': payload.invoiceNo
            }
        }
    };

    try {
        const resp = await searchDoc(getEsPaymentIndex, body);
        let result = resp['hits']['hits'];

        if(result.length == 0){
            logger.info('No rusults found for the reference number.');
            return {ok:true,status:404,message:'No rusults found for the reference number.',data:{}};
        }

        let bodyIn = result[0];
        if(bodyIn !== null) {
            const depositResponse = await manageDeposit(bodyIn._source);
            if (!depositResponse.ok) {
                return {
                    ok: true, status: 406, message: 'Error with Telkom Core manageDepositApi.', data: {
                        errorDetails: depositResponse.message
                    }
                };
            }

            const resp = await searchDoc(getEsRefundIndex, body);
            let result = resp['hits']['hits'];
            if (result.length == 0 || _.isEmpty(result)) {
                logger.error('No results found for the invoice number.');
                return {ok: true, status: 404, message: 'No results found for the invoice number.', data: {}};
            }

            let refund = result[0];
            if (refund !== null) {
                refund._source.accountHolder = payload.accountHolder;
                refund._source.bank = payload.bank;
                refund._source.accountNumber = payload.accountNumber;

                try {
                    await updateDoc(getEsRefundIndex, refund._id, refund._source);
                    return {ok: true, status: 200, message: 'Success updating payment data.', data: {}};
                } catch (e) {
                    logger.error(`Error updating refund data. Error: ${JSON.stringify(e)}`);
                    return {ok: false, status: 406, message: 'Error updating refund data. Error.', data: {}};
                }
            }
        }

    }catch(e){
        logger.error('Error fetching refund data.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error fetching refund data.',data:{}};
    }
};

/**
 * transfer from t-money to linkaja
 * @param {String} invoice - invoice number
 * @returns {Promise<{data: *, ok: boolean, message: string, status: number}|{data: {}, ok: boolean, message: string, status: number}>}
 */
module.exports.getRefundPayment = async (invoice) => {

    let body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'match': {
                            'invoice': payload.invoice
                        }
                    }
                ]
            }
        }
    };

    try {
        const resp = await searchDoc(getEsRefundIndex, body);
        let result = resp['hits']['hits'];
        if(result.length == 0 || _.isEmpty(result)){
            logger.error('No results found for the invoice number.');
            return {ok:true,status:404,message:'No results found for the invoice number.',data:{}};
        }
        const respData = result.map(el => el._source);
        return {ok:true,status:200,message:'Successfully received refund data.',data: respData};

    }catch(e){
        logger.error('Error fetching refund data.',JSON.stringify(e));
        return {ok:false,status:500,message:'Error fetching refund data.',data:{}};
    }
};







