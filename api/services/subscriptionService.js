/*
 * Copyright (c) 2020. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 3/13/20, 10:18 AM
 */

const rp = require('request-promise');
const _ = require('lodash');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('Subscription Service');

const config = require('../config');
const {
    host, getSoDSubscriptionEndpoint, activateSoDEndpoint, getInternetSubscriptionEndpoint, activateUpgradeSpeedEndpoint,
    getMiniPackSubscriptionEndpoint, activateMiniPackEndpoint
} = config.get('/subscriptionService');

const serviceAuthToken = config.get('/authorization').token;

module.exports.getSoDSubscription = async transactionId => {
    const options = {
        method: 'GET',
        uri: `${host}${getSoDSubscriptionEndpoint}${transactionId}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.get(options);
        logger.info(`get user SoD subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};

module.exports.activateSoDPackage = async transactionId => {
    const options = {
        method: 'POST',
        uri: `${host}${activateSoDEndpoint}${transactionId}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.post(options);
        logger.info(`activating user SoD subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};

module.exports.activateUpgradeSpeedPackage = async transactionId => {
    const options = {
        method: 'POST',
        uri: `${host}${activateUpgradeSpeedEndpoint}${transactionId}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.post(options);
        logger.info(`activating user Upgrade Speed subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};

module.exports.getSelectedInternetPackage = async transactionId => {
    const options = {
        method: 'GET',
        uri: `${host}${getInternetSubscriptionEndpoint}${transactionId}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.get(options);
        logger.info(`get user Internet subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};

/**
 * Get minipack subscription by transactionId
 * @param {String} transactionId - transaction Id
 * @returns {Promise<null|*>}
 */
module.exports.getMiniPackSubscription = async transactionId => {
    const options = {
        method: 'GET',
        uri: `${host}${getMiniPackSubscriptionEndpoint}`.replace(':transactionId', transactionId),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.get(options);
        logger.info(`get user Minipack subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};

/**
 * Activate minipack subscription
 * @param {String} transactionId - transaction Id
 * @returns {Promise<null|*>}
 */
module.exports.activateMiniPackPackage = async transactionId => {
    const options = {
        method: 'POST',
        uri: `${host}${activateMiniPackEndpoint}`.replace(':transactionId', transactionId),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Basic ${serviceAuthToken}`,
        },
        json: true,
        strictSSL: false
    };

    try {
        let response = await rp.post(options);
        logger.info(`activating user Minipack subscription Request: ${JSON.stringify(options)} Response: ${JSON.stringify(response)}`);
        if (!_.isEmpty(response.data)) {
            return response.data;
        }
    } catch (reason) {
        logger.error('error', reason);
    }
    return null;
};
