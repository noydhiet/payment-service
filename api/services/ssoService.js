const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('NotificationService');
const config = require('../config');
const identityServerUrl = config.get('/ssoService');


/**
 * Get user by userId
 * @param {String} userId - userId
 * @returns {Promise<{err: null, data: *}>}
 */
module.exports.getUserSSOUserData = async(userId) => {
    const options = {
        method: 'GET',
        uri: identityServerUrl.host +'/user/userSsoProfile/'+ userId,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: 'Basic ' + config.get('/authorization').token,
        },
        json: true,
        strictSSL: false
    };
    logger.info('Get user profile of the user');
    try {
        const result = await rp.get(options);
        return result;
    } catch (err) {
        return null;
    }
};