/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/10/2020
 */
const _ = require('lodash');
const moment = require('moment');
const joi = require('@hapi/joi');
const validator = require('../helpers/utils/validator');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('rewardService');
const config = require('../config');
const {InternalServerError, BadRequestError, ForbiddenError} = require('../helpers/error');
const rp = require('request-promise');
const sha1 = require('sha1');
const AuthenticationService = require('../middleware/telkom_jwt');
const authenticationService = new AuthenticationService();
const userProfileService = require('../services/userProfileService');

const {getPointsRedeemHistoryEndpoint, getPointsHistoryEndpoint, redeemPointsEndpoint, getPointDetailsEndpoint,
    checkPointsEndpoint, getPointsListEndpoint, rewardsESIndex, rewardsTypesESIndex,
    rewardsPreferencesESIndex, getVouchersEndpoint, useVoucherEndpoint,
} = config.get('/rewardService');
const size = config.get('/elasticSearch/size');

const insertData = require('../helpers/databases/elasticSearch/insertData');
const searchData = require('../helpers/databases/elasticSearch/searchData');
const updateData = require('../helpers/databases/elasticSearch/updateData');

const {POINT_KEY, POINT_SOURCE, REWARDS_ACTIVE_STATUS,
    LIST_VOUCHER_OP
} = require('../helpers/utils/constants/rewardConstants');
const {DATE_FORMATS, HTTP_STATUS} = require('../helpers/utils/constants/commonConstants');
const ssoService = require('./ssoService');

/**
 * Get Points Expiry data for user
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getPointsExpiry = async (selectedIndiHomeNum) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if(_.isEmpty(selectedIndiHomeNum)){
        throw new BadRequestError('Empty selectedIndiHomeNum.');
    }
    const options = {
        method: 'POST',
        uri: getPointsHistoryEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: {
            ND: selectedIndiHomeNum,
            page: '1'
        },
        strictSSL: false
    };

    logger.info(`Sending request to get points expiry data. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get points expiry data. Response: ${JSON.stringify(response)}`);

        let responseData = [];
        if(_.isEmpty(response.data)) {
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        responseData = response.data.OUT_RESULT.filter(el => el.status === 'plus')
            .map(el => {
                const updatedDate = moment(el.updated_date, DATE_FORMATS.PLAIN_DATETIME_REVERSE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT);
                const obj = {
                    points: `${el.poin} pts`,
                    expireDate: moment(updatedDate).endOf('year').format(DATE_FORMATS.ISO_DATE_FORMAT),
                };
                const now = new Date().toISOString();
                obj.isExpired = moment(moment(now, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT)).isAfter(obj.expireDate, 'minute');
                return obj;
            }).sort((a, b) => new Date(b.expireDate) - new Date(a.expireDate)); // descending order by point expiry date
        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get point expiry data. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get point expiry data. Response: ${JSON.stringify(response)}`);
};

/**
 * Get Points history data for user
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getPointsHistory = async (selectedIndiHomeNum) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if(_.isEmpty(selectedIndiHomeNum)){
        throw new BadRequestError('Empty selectedIndiHomeNum.');
    }
    const options = {
        method: 'POST',
        uri: getPointsHistoryEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: {
            ND: selectedIndiHomeNum,
            page: '1'
        },
        strictSSL: false
    };

    logger.info(`Sending request to get points history data. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get points history data. Response: ${JSON.stringify(response)}`);
        let responseData = [];
        if(_.isEmpty(responseData)) {
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        responseData = response.data.OUT_RESULT.map(el => {
            const updatedDate = moment(el.updated_date, DATE_FORMATS.PLAIN_DATETIME_REVERSE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT);
            return {
                points: el.poin,
                type: el.status,
                date: updatedDate,
                expireDate: moment(updatedDate).endOf('year').format(DATE_FORMATS.ISO_DATE_FORMAT),
                description: el.descriptions

        };
        }).sort((a, b) =>  new Date(b.date) - new Date(a.date)); // descending order by point used/added date
        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get point history data. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get point history data. Response: ${JSON.stringify(response)}`);
};

/**
 * Get Points history data for user
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @param {String} email - user email
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getPointsRedeemHistory = async (selectedIndiHomeNum, userId) => {

    if (!userId) {
        throw new BadRequestError('Invalid userId');
    }
    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }
    let email = userData.data.email;
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if(_.isEmpty(selectedIndiHomeNum)){
        throw new BadRequestError('Empty selectedIndiHomeNum.');
    }

    // Get user account data by indiHome number
    const account = await userProfileService.getUserAccount(selectedIndiHomeNum);
    if (account.err) {
        logger.error(`Error in getting account data. Response: ${JSON.stringify(account)}`);
        throw new InternalServerError('Error in getting account data');
    }
    logger.info(`Received account data: ${JSON.stringify(account)}`);
    const accountData = account.data;

    const options = {
        method: 'POST',
        uri: getPointsRedeemHistoryEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: {
            ncli: accountData.ncli,
            email: email
        },
        strictSSL: false
    };

    logger.info(`Sending request to get points redeem history data. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get points redeem history data. Response: ${JSON.stringify(response)}`);
        let responseData = [];
        if(_.isEmpty(response.data)) {
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        responseData = response.data.map(el => {
            return {
                eventId: el.EVENT_ID,
                eventDate: moment(el.EVENT_DTM, DATE_FORMATS.PLAIN_DATETIME_ZONE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),//
                description: el.DESCRIPTION,
                redeemKey: el.REQUEST,
                voucher: el.RESPONSE,
                imageUrl: el.DATA2
            };
        });
        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get point redeem history data. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get point redeem history data. Response: ${JSON.stringify(response)}`);
};

/**
 * Check Points for indiHome Number
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.checkPoints = async (selectedIndiHomeNum) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if(_.isEmpty(selectedIndiHomeNum)){
        throw new BadRequestError('Empty selectedIndiHomeNum.');
    }
    const options = {
        method: 'POST',
        uri: checkPointsEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: {
            ND: selectedIndiHomeNum
        },
        strictSSL: false
    };

    logger.info(`Sending request to get points summary. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get points summary. Response: ${JSON.stringify(response)}`);
        const responseData = {
            totalPoints: response.data.OUT_RESULT[0].poin_total,
            pointsCurrentMonth: response.data.OUT_RESULT[0].poin_cm
        };

        //get latest expiry data
        const pointExpiry = await module.exports.getPointsExpiry(selectedIndiHomeNum);
        if(pointExpiry.err) {
            logger.error(`Error in getting point expiry data. ${pointExpiry.err}`);
        }
        const willExpire = pointExpiry.data.filter(el => el.isExpired === false);
        const latestExpiry = !_.isEmpty(willExpire) ? willExpire[0] : null;
        responseData.latestExpiry = latestExpiry;

        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get point summary. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get point summary. Response: ${JSON.stringify(response)}`);
};

/**
 * Get rewards list
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @param {Object} userData - user data Objs
 * @param {String} filterBy - filterBy reward category
 * @param {Boolean} group - group by reward category
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getRewardsList = async (selectedIndiHomeNum, userData, filterBy= undefined, group = false) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;
    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('indiHomeNum and pointId cannot be empty.');
        throw new BadRequestError('Empty selectedIndiHomeNum and pointId');
    }

    if(group && !['true', 'false'].includes(group)){
        logger.error('Invalid group given.');
        throw new BadRequestError('Invalid group given.');
    }

    group = (group === 'true')? true : false;

    if(filterBy !== undefined && _.isEmpty(filterBy)){
        logger.error('If passed, filterBy must have a value');
        throw new BadRequestError('If passed, filterBy must have a value');
    }

    const rewardTypes = await getRewardsTypesArray();
    if(filterBy && !rewardTypes.includes(filterBy)){
        logger.error('Invalid filterBy given.');
        throw new BadRequestError('Invalid filterBy given.');
    }
    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received user profile.`);
    const account = profile && profile.accounts? profile.accounts.find(ac => ac.indiHomeNum === selectedIndiHomeNum): null;

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile.`);
        throw new InternalServerError('No matching account found in profile');
    }
    const reqBody =  {
        ncli: account.ncli,
        source: POINT_SOURCE,
        email: account.email,
        nd: account.indiHomeNum,
        msisdn: profile.primaryPhone,
        key: POINT_KEY,
        signature: sha1(account.email + account.indiHomeNum + profile.primaryPhone + "myindihome") + "1nd1P45s"
    };

    const options = {
        method: 'POST',
        uri: getPointsListEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to get rewards list. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get rewards list. Response length: ${response.length}`);
        let responseData = [];
        if(_.isEmpty(response.data)) {
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        let now = moment().endOf('day').format(DATE_FORMATS.ISO_DATE_FORMAT);
        let allActiveRewards = response.data
            .filter(el => moment(moment(el.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT)).isAfter(now, 'minute'))
            .map(el => mapRewardObj(el));
        // save rewards list in DB
        saveRewards(allActiveRewards);
        if(_.isEmpty(filterBy) && account.rewardsActive) {
            // filter as per user's rewards preferences if activated
            logger.info('Not given filterBy. Filtering rewards by rewards preferences.');

            let rewardsPreferences = await getRewardsPreference(selectedIndiHomeNum);
            if (!rewardsPreferences) {
                logger.error(`Error in getting rewards preferences. Response: ${JSON.stringify(rewardsPreferences)}`);
            }
            logger.info(`Received rewards preferences: ${JSON.stringify(rewardsPreferences)}`);
            allActiveRewards = allActiveRewards.filter(el => rewardsPreferences.preferences.includes(el.categoryCode));
        }
        if(group) {
            logger.info('group by reward types ON');

            // group by and sort group by expireDate
            responseData = _.mapValues(_.groupBy(allActiveRewards, 'categoryCode'),
                data => data
                    .sort((a, b) => new Date(a.expireDate) - new Date(b.expireDate))
            );
            // order by category/reward code asc
            const ordered = {};
            _(responseData).keys().sort().each(key => {
                ordered[key] = responseData[key];
            });
            responseData = ordered;
        } else {
            if(_.isEmpty(filterBy)) {
                responseData = allActiveRewards
                    .sort((a, b) => (a.categoryName > b.categoryName)? 1: -1);
            } else {
                logger.info(`filter by reward type ${filterBy}`);
                responseData = allActiveRewards.filter(el => filterBy === el.categoryCode)
                    .sort((a, b) => new Date(a.expireDate) - new Date(b.expireDate));
            }
        }

        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get rewards list. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get rewards list. Response: ${JSON.stringify(response)}`);

};

/**
 * Get recommended Rewards list
 *
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @param {Object} userData - user data Objs
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getRecommendedRewards = async (selectedIndiHomeNum, userData) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;
    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('indiHomeNum and pointId cannot be empty.');
        throw new BadRequestError('Empty selectedIndiHomeNum and pointId');
    }

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received user profile.`);
    const account = profile && profile.accounts? profile.accounts.find(ac => ac.indiHomeNum === selectedIndiHomeNum): null;

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile.`);
        throw new InternalServerError('No matching account found in profile');
    }
    const reqBody =  {
        ncli: account.ncli,
        source: POINT_SOURCE,
        email: account.email,
        nd: account.indiHomeNum,
        msisdn: profile.primaryPhone,
        key: POINT_KEY,
        signature: sha1(account.email + account.indiHomeNum + profile.primaryPhone + "myindihome") + "1nd1P45s"
    };

    const options = {
        method: 'POST',
        uri: getPointsListEndpoint, //todo: replace with new coreAPI once given
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to get recommended rewards list. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get recommended rewards list. Response length: ${response.length}`);
        let responseData = [];
        if(_.isEmpty(response.data)) {
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        let now = moment().endOf('day').format(DATE_FORMATS.ISO_DATE_FORMAT);
        let allActiveRewards = response.data
            .filter(el => el.is_recommended === '1' && moment(moment(el.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT)).isAfter(now, 'minute'))
            .map(el => mapRewardObj(el));
        if(account.rewardsActive) {
            // filter as per user's rewards preferences if activated
            logger.info('Filtering rewards by rewards preferences.');

            let rewardsPreferences = await getRewardsPreference(selectedIndiHomeNum);
            if (!rewardsPreferences) {
                logger.error(`Error in getting rewards preferences. Response: ${JSON.stringify(rewardsPreferences)}`);
            }
            logger.info(`Received rewards preferences: ${JSON.stringify(rewardsPreferences)}`);
            allActiveRewards = allActiveRewards.filter(el => rewardsPreferences.preferences.includes(el.categoryCode));
        }
        responseData = allActiveRewards
            .sort((a, b) => (a.categoryName > b.categoryName)? 1: -1);

        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get recommended rewards list. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get recommended rewards list. Response: ${JSON.stringify(response)}`);

};

/**
 * Format reward object
 */
function mapRewardObj(el) {
    return {
        pointId: el.id,
        title: el.title,
        description: el.description,
        createdDate: moment(el.create_time, DATE_FORMATS.PLAIN_DATETIME_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
        price: el.price,
        imageUrl: el.picture,
        expireDate: moment(el.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
        redeemMethod: el.is_voucher === '1'? 'redeemKey' : 'QR' ,
        isDelivery: el.is_delivery === '1'? 1 : 0,
        isChild: el.is_child === '1'? 1 : 0,
        isRecommended: el.is_recommended === '1'? 1 : 0,
        isStore: el.is_store === '1'? 1 : 0,
        categoryCode: el.category_name,
        categoryName: properCase(el.category_name)
    };
}

/**
 * Format voucher object
 */
function mapVoucherObj(el) {
    return {
        id: el.id,
        title: el.title,
        pointTitle: el.title_point,
        description: el.description,
        createdDate: moment(el.create_time, DATE_FORMATS.PLAIN_DATETIME_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
        imageUrl: el.picture,
        expireDate: moment(el.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
        redeemMethod: el.is_voucher === '1'? 'redeemKey' : 'QR' , //todo asked to add this field to API
        redeemKey: el.redeem_key,
        isExpired: el.is_expired === '1'? 1 : 0,
        isRedeemed: el.used_merchant !== null? 1 : 0,
        isStore: el.is_store === '1'? 1 : 0
    };
}

/**
 * Get point details by point Id
 *
 * @param {String} pointId -  point Id
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getPointDetails = async (pointId, selectedIndiHomeNum) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;
    if(_.isEmpty(selectedIndiHomeNum) || _.isEmpty(pointId)){
        logger.error('indiHomeNum and pointId cannot be empty.');
        throw new BadRequestError('Empty selectedIndiHomeNum and pointId');
    }

    // Get user account data by indiHome number
    let account = await userProfileService.getUserAccount(selectedIndiHomeNum);
    if (account.err) {
        logger.error(`Error in getting account data. Error: ${JSON.stringify(account)}`);
        throw new InternalServerError('Error in getting account data');
    }

    account = account.data;
    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Account Data: ${JSON.stringify(account)}`);
        throw new InternalServerError('No matching account found in profile');
    }

    const reqBody =  {
        key: POINT_KEY,
        ncli: account.ncli,
        id: pointId,
        email: account.email
    };

    const options = {
        method: 'POST',
        uri: getPointDetailsEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to get point details. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`Received response from get point details. Response: ${JSON.stringify(response)}`);
        let responseData = {};
        if(_.isEmpty(response.data)){
            return {
                status: HTTP_STATUS.SUCCESS,
                data: responseData
            };
        }
        responseData = {
            pointId: response.data.id,
            merchant: response.data.merchant,
            merchantImgUrl: response.data.merchant_picture,
            title: response.data.title,
            description: response.data.description,
            createdDate: moment(response.data.create_time, DATE_FORMATS.PLAIN_DATETIME_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
            ValidDays: response.data.ValidDays,
            isChild: response.data.is_child === '1'? 1 : 0,
            price: response.data.price,
            imageUrl: response.data.picture,
            expireDate: moment(response.data.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT),
            redeemMethod: response.data.is_voucher === '1'? 'redeemKey' : 'QR' ,
            isDelivery: response.data.is_delivery === '1'? 1 : 0,
            redeemKey: response.data.reedem_key
        };
        // update reward data (redeem Key)
        updateReward(responseData);
        return {
            status: HTTP_STATUS.SUCCESS,
            data: responseData
        };
    }

    logger.error(`Error in get point details. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in get point details. Response: ${JSON.stringify(response)}`);
};

/**
 * Redeem points
 *
 * @param {Object} redeemData - redeem request data
 * @param {String} selectedIndiHomeNum -  selected IndiHome Number
 * @param {Object} userData - user data
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.redeemPoints = async (redeemData, selectedIndiHomeNum, userData) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if(_.isEmpty(selectedIndiHomeNum)) {
        logger.error('indiHomeNum cannot be empty.');
        throw new BadRequestError('indiHomeNum cannot be empty.');
    }
    const validatePayload = validator.isValidPayload(redeemData, redeemDataModel);
    if(validatePayload.err) {
        logger.error(`Invalid payload. Error ${validatePayload.err}`);
        throw new BadRequestError(`Invalid payload. Error: ${validatePayload.err}`);
    }

    redeemData = validatePayload.data;

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`received user profile. Profile data: ${JSON.stringify(profile)}`);
    const account = profile && profile.accounts? profile.accounts.find(ac => ac.indiHomeNum === selectedIndiHomeNum): null;

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('No matching account found in profile');
    }
    const reqBody =  {
        key: POINT_KEY,
        ncli: account.ncli,
        source: POINT_SOURCE,
        email: account.email,
        nd: account.indiHomeNum,
        msisdn: profile.primaryPhone,
        signature: sha1(account.email + account.indiHomeNum + profile.primaryPhone + "myindihome") + "1nd1P45s",
        id: redeemData.pointId,
        redeemkey: redeemData.redeemKey
    };

    const options = {
        method: 'POST',
        uri: redeemPointsEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to redeem points. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.code === '0') {
        logger.info(`Received response from redeem points. Response: ${JSON.stringify(response)}`);

        return {
            status: HTTP_STATUS.SUCCESS,
            data: response.data,
            message: response.info
        };
    }

    logger.error(`Error in redeem points. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in redeem points. Response: ${JSON.stringify(response)}`);
};

/**
 * Use voucher
 *
 * @param {Object} voucherData - use voucher request data
 * @param {String} indiHomeNum -  selected IndiHome Number
 * @param {Object} userData - user data
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.useVoucher = async (voucherData, indiHomeNum, userData) => {
    if(_.isEmpty(indiHomeNum)) {
        logger.error('indiHomeNum cannot be empty.');
        throw new BadRequestError('indiHomeNum cannot be empty.');
    }
    if(_.isEmpty(userData)) {
        logger.error('userData cannot be empty.');
        throw new ForbiddenError('Unauthorized action. User data cannot be empty.');
    }
    const validatePayload = validator.isValidPayload(voucherData, voucherDataModel);
    if(validatePayload.err) {
        logger.error(`Invalid payload. Error ${validatePayload.err}`);
        throw new BadRequestError(`Invalid payload. Error: ${validatePayload.err}`);
    }

    voucherData = validatePayload.data;

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`received user profile. Profile data: ${JSON.stringify(profile)}`);
    const account = profile && profile.accounts? profile.accounts.find(ac => ac.indiHomeNum === indiHomeNum): null;

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('No matching account found in profile');
    }
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    const reqBody =  {
        key: POINT_KEY,
        ncli: account.ncli,
        source: POINT_SOURCE,
        email: account.email,
        nd: account.indiHomeNum,
        msisdn: profile.primaryPhone,
        signature: sha1(account.email + account.indiHomeNum + profile.primaryPhone + "myindihome") + "1nd1P45s",
        id: voucherData.voucherId,
        redeemkey: voucherData.redeemKey
    };

    const options = {
        method: 'POST',
        uri: useVoucherEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to redeem points. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.code === '0') {
        logger.info(`Received response from redeem points. Response: ${JSON.stringify(response)}`);

        return {
            status: HTTP_STATUS.SUCCESS,
            data: response.data,
            message: response.info
        };
    }

    logger.error(`Error in redeem points. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in redeem points. Response: ${JSON.stringify(response)}`);
};

/**
 * Get rewards code array
 * @returns {Promise<*|*[]>}
 */
const getRewardsTypesArray = async() => {
    const rewardTypes = await getRewardTypes();
    return rewardTypes.map(el => el.name) || [];
};

/**
 * Get rewards Types and names
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getRewardsTypes = async() => {
    const rewardTypes = await getRewardTypes();
    let response = rewardTypes.map(el => {
        return {
            code: el.name,
            name: properCase(el.name)
        }
    });
    return {
        status: HTTP_STATUS.SUCCESS,
        data: response
    }
};

/**
 * Get reward Type by Id
 * @param {String} id - rewards type id
 * @returns {Promise<{data: *, status: string}>}
 */
module.exports.getRewardsType = async(id) => {
    if(_.isEmpty(id)) {
        logger.error('Invalid Id.');
        throw new BadRequestError('Invalid Id.');
    }
    const rewardType = await getRewardType(id);
    return {
        status: HTTP_STATUS.SUCCESS,
        data: rewardType
    }
};

/**
 * Save new rewards
 * @param {Object} payload - reward type payload
 * @returns {Promise<*>}
 */
module.exports.addRewardsType = async(payload) => {
    const validatePayload = validator.isValidPayload(payload, rewardTypeDataModel);
    if(validatePayload.err) {
        logger.error(`Invalid payload. Error ${validatePayload.err}`);
        throw new BadRequestError(`Invalid payload. Error: ${validatePayload.err}`);
    }

    payload = validatePayload.data;

    try {
        const resp = await insertData.insertDoc(rewardsTypesESIndex, '_doc', payload, payload.id);
        logger.info(`Successfully saved reward type. Response: ${JSON.stringify(resp)}`);
        return resp;
    } catch (e) {
        logger.error('Error saving reward type data.', e);
        throw new InternalServerError('Error in saving reward type.');}
};

/**
 * Update Rewards Type
 * @param {Object} payload - reward type payload
 * @param {String} id - rewards type id
 * @returns {Promise<*>}
 */
module.exports.updateRewardsType = async(payload, id) => {
    if(_.isEmpty(id)) {
        logger.error('Id cannot be empty.');
        throw new BadRequestError('Id cannot be empty.');
    }
    if(_.isEmpty(payload)) {
        logger.error('Payload cannot be empty.');
        throw new BadRequestError('Payload cannot be empty.');
    }
    const validatePayload = validator.isValidPayload(payload, updateRewardTypeDataModel);
    if(validatePayload.err) {
        logger.error(`Invalid payload. Error ${validatePayload.err}`);
        throw new BadRequestError(`Invalid payload. Error: ${validatePayload.err}`);
    }

    payload = validatePayload.data;

    let updateRewardType = await getRewardType(id);

    if(updateRewardType !== null) {
        try {
            updateRewardType = Object.assign(updateRewardType, payload);
            const resp = await updateData(rewardsTypesESIndex, id, updateRewardType);
            logger.info(`Successfully updated reward type. Response: ${JSON.stringify(resp)}`);
            return resp;
        } catch (e) {
            logger.error('Error updating reward type data.', e);
            throw new InternalServerError('Error in updating reward type.');
        }
    }
};

/**
 * Activate rewards
 * @param {String} indiHomeNum - indiHome Number
 * @param {Array} selectedTypes - selected rewards category array
 * @param {Object} userData - user data
 * @returns {Promise<{data: {indiHomeNum: *, status: string}, message: string, status: string}>}
 */
module.exports.activateRewards = async (indiHomeNum, selectedTypes, userData) => {
    if(_.isEmpty(userData)) {
        logger.error(`Invalid userData. ${JSON.stringify(userData)}`);
        throw new ForbiddenError('Forbidden action. Invalid userData');
    }
    if(_.isEmpty(indiHomeNum)) {
        logger.error('indiHomeNum cannot be empty.');
        throw new BadRequestError('indiHomeNum cannot be empty.');
    }
    const rewardTypes = await getRewardsTypesArray();
    if(_.isEmpty(selectedTypes) || !_.isArray(selectedTypes) || _.isEmpty(_.intersection(rewardTypes, selectedTypes))) {
        logger.error('invalid selectedTypes.');
        throw new BadRequestError('Invalid selectedTypes.');
    }

    // activate rewards in account
    const activated = await userProfileService.setRewardsActiveStatus(indiHomeNum, REWARDS_ACTIVE_STATUS.ACTIVE);
    if(activated.err) {
        logger.error('Error in activating rewards in account', activated.err);
        throw new InternalServerError(`Error in activating rewards in account. Response: ${activated}`);
    }
    const now = new Date().toISOString();
    // save preferences
    const rewardPreference = {
        userId: userData.userId,
        indiHomeNum,
        active: true,
        preferences: selectedTypes,
        createdAt: now,
        updatedAt: now
    };

    const saved = await saveRewardsPreference(rewardPreference);
    if(saved.err) {
        logger.error('Error in saving user rewards preferences.', saved.err);
        throw new InternalServerError('Error in saving user rewards preferences.');
    }

    return {
        status: HTTP_STATUS.SUCCESS,
        data: {
            indiHomeNum,
            status: REWARDS_ACTIVE_STATUS.ACTIVE
        },
        message: 'Activated rewards.'
    };
};

/**
 * Convert string to sentence case
 * @param {String} el - string to convert
 * @returns {*}
 */
function properCase(el) {
    return el.split(' ').map(sub=>_.capitalize(sub)).join(' ');
}

/**
 * Save rewards preferences
 * @param {Object} payload - payload
 * @returns {Promise<void>}
 */
async function saveRewardsPreference(payload) {
    try {
        const resp = await insertData.insertDoc(rewardsPreferencesESIndex, '_doc', payload, payload.indiHomeNum);
        logger.info(`Successfully saved reward preferences. Response: ${JSON.stringify(resp)}`);
        return resp;
    } catch (e) {
        logger.error('Error saving reward preferences data.', e);
        return ;
    }
}

/**
 * get rewards preferences
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<void>}
 */
async function getRewardsPreference(indiHomeNum) {
    const body = {
        query: {
            match : {
                indiHomeNum
            }
        }
    };
    const resp = await searchData(rewardsPreferencesESIndex, body);
    let result = resp['hits']['hits'];
    if(result.length === 0){
        logger.error(`Not found rewards preference data for indiHomeNum: ${indiHomeNum}`);
    }

    let data = result[0];
    if(!_.isEmpty(data)) {
        return data._source;
    }
    return null;
}

/**
 * Get vouchers by account
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<*[]|*>}
 */
module.exports.getVouchers = async (indiHomeNum, userData) => {
    if(_.isEmpty(indiHomeNum)) {
        logger.error('indiHomeNum cannot be empty.');
        throw new BadRequestError('indiHomeNum cannot be empty.');
    }

    if(_.isEmpty(userData)) {
        logger.error('userData cannot be empty.');
        throw new ForbiddenError('Unauthorized action. User data cannot be empty.');
    }

    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    // Get user account data by indiHome number
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        logger.error(`Error in getting profile data. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`received user profile. Profile data: ${JSON.stringify(profile)}`);
    const account = profile && profile.accounts? profile.accounts.find(ac => ac.indiHomeNum === indiHomeNum): null;

    if(_.isEmpty(account)) {
        logger.error(`No matching account found in profile. Error: ${JSON.stringify(profile)}`);
        throw new InternalServerError('No matching account found in profile');
    }

    let reqBody =  {
        key: POINT_KEY,
        ncli: account.ncli,
        email: account.email,
        nd: account.indiHomeNum,
        msisdn: profile.primaryPhone,
        signature: sha1(account.email + account.indiHomeNum + profile.primaryPhone + "myindihome") + "1nd1P45s",
        source: POINT_SOURCE,
        operation: LIST_VOUCHER_OP
    };

    const options = {
        method: 'POST',
        uri: getVouchersEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Sending request to get user's vouchers. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.code === '0') {
        logger.info(`Received response getting user's vouchers. Response: ${JSON.stringify(response)}`);
        let now = moment().endOf('day').format(DATE_FORMATS.ISO_DATE_FORMAT);

        let usersVouchers = [];
        if(!_.isEmpty(response.data)) {
            usersVouchers = response.data
                .filter(el => moment(moment(el.expired, DATE_FORMATS.PLAIN_DATE_FORMAT).format(DATE_FORMATS.ISO_DATE_FORMAT)).isAfter(now, 'minute'))
                .map(el => mapVoucherObj(el))
                .sort((a, b) => new Date(a.expireDate) - new Date(b.expireDate));

        }

        return {
            status: HTTP_STATUS.SUCCESS,
            data: usersVouchers,
        };
    }

    logger.error(`Error in getting user's vouchers. Date: ${new Date()}. Response: ${JSON.stringify(response)}`);
    throw new InternalServerError(`Error in getting user's vouchers. Response: ${JSON.stringify(response)}`);
};

/**
 * Save rewards list bulk on ES
 * @param {Object} rewardsData - rewards data
 * @returns {Promise<void>}
 */
async function saveRewards(rewardsData) {
    let rewardsRecords = '';

    try {
        rewardsData.forEach((doc, idx) => {
            rewardsRecords = rewardsRecords
                .concat(JSON.stringify({index: {'_index': rewardsESIndex, '_id': doc.pointId}}))
                .concat('\n').concat(JSON.stringify(doc));
            if (idx < rewardsData.length - 1) {
                rewardsRecords = rewardsRecords.concat('\n');
            }
        });
        const resp = await insertData.insertBulkDoc(rewardsRecords);
        logger.info(`Successfully saved rewards list. Response: ${JSON.stringify(resp)}`);
    } catch (e) {
        logger.error('Error saving rewards list data.', e);
    }
}

/**
 * Get reward types list
 * @returns {Promise<null|*>}
 */
async function getRewardTypes() {
    const body = {
        from:0,
        size: size,
        query: {
            match: {
                active: true
            }
        }
    };
    const resp = await searchData(rewardsTypesESIndex, body);
    let result = resp['hits']['hits'];
    if(result.length === 0){
        logger.error('Error in getting rewards types.');
    }

    if(!_.isEmpty(result)) {
        return result.map(el => el._source);
    }
    return null;
}


/**
 * Get a reward type by id
 * @param {String} id - id of reward type
 * @returns {Promise<null|*>}
 */
async function getRewardType(id) {
    const body = {
        query: {
            match : {
                id
            }
        }
    };
    const resp = await searchData(rewardsTypesESIndex, body);
    let result = resp['hits']['hits'];
    if(result.length === 0){
        logger.error(`Not found reward type data for id: ${id}`);
    }

    let reward = result[0];
    if(!_.isEmpty(reward)) {
        return reward._source;
    }
    return null;
}


/**
 * Get reward by pointId
 * @param {String} pointId - pointId
 * @returns {Promise<null|*>}
 */
async function getReward(pointId) {
    const body = {
        query: {
            match : {
                pointId: pointId
            }
        }
    };
    const resp = await searchData(rewardsESIndex, body);
    let result = resp['hits']['hits'];
    if(result.length === 0){
        logger.error(`Not found rewards data for pointId: ${pointId}`);
    }

    let reward = result[0];
    if(!_.isEmpty(reward)) {
        return reward._source;
    }
    return null;
}

/**
 * Update rewards
 * @param {Object} rewardData - existing reward data
 * @returns {Promise<void>}
 */
async function updateReward(rewardData) {
    let updateReward = await getReward(rewardData.pointId);
    if(updateReward !== null) {
        try {
            updateReward = Object.assign(rewardData, updateReward);
            const updated = await updateData(rewardsESIndex, '_doc', updateReward.pointId, updateReward);
            logger.info(`Successfully updated reward. Response: ${JSON.stringify(updated)}`);
        } catch (e) {
            logger.error('Error updating reward data.', e);
        }
    } else {
        logger.error('Error in getting reward data. Null values.');
    }
}

const redeemDataModel = joi.object({
    pointId: joi.string().required(),
    redeemKey: joi.string().required()
});

const voucherDataModel = joi.object({
    voucherId: joi.string().required(),
    redeemKey: joi.string().required()
});

const rewardTypeDataModel = joi.object({
    id: joi.string().required(),
    name: joi.string().required(),
    active: joi.boolean().required(),

});

const updateRewardTypeDataModel = joi.object({
    name: joi.string().when('', {is: joi.exist(), then: joi.required()}),
    active: joi.boolean().when('', {is: joi.exist(), then: joi.required()}),
});

