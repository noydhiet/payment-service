/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 26/11/19
*/
const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('ProfileService');
const wrapper = require('../helpers/utils/wrapper');
const {InternalServerError} = require('../helpers/error');
const config = require('../config');
const userProfileServiceUrl = config.get('/userProfileService');

/**
 * Get account by indiHomeNum
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<{err: null, data: *}>}
 */
module.exports.getUserAccount = async(indiHomeNum) => {
    const options = {
        method: 'GET',
        uri: userProfileServiceUrl.host + userProfileServiceUrl.getUserAccountEndpoint.replace(':indiHomeNum', indiHomeNum),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: 'Basic ' + config.get('/authorization').token,
        },
        json: true,
        strictSSL: false
    };
    logger.info('Get user profile of the user');
    const result = await rp.get(options);
    if (result.err) {
        logger.error(`Error in getting profile data: ${JSON.stringify(result.err)}`);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received profile data: ${JSON.stringify(result.data)}`);
    return wrapper.data(result.data);
};

/**
 * Set rewards active status by indiHomeNum
 * @param {String} indiHomeNum - indiHome Number
 * @param {String} status - Status [active, inactive]
 * @returns {Promise<{err: null, data: *}>}
 */
module.exports.setRewardsActiveStatus = async(indiHomeNum, status) => {
    const options = {
        method: 'PUT',
        uri: userProfileServiceUrl.host + userProfileServiceUrl.rewardsActiveStatusEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: 'Basic ' + config.get('/authorization').token,
        },
        json: {
            indiHomeNum: indiHomeNum,
            status: status
        },
        strictSSL: false
    };
    logger.info('Setting rewards active status of the user');
    const result = await rp.put(options);
    if (result.err) {
        logger.error(`Error in setting rewards active status: ${JSON.stringify(result.err)}`);
        throw new InternalServerError('Error in setting rewards active status');
    }

    logger.info(`Successfully updated rewards active status: ${JSON.stringify(result.data)}`);
    return wrapper.data(result.data);
};

/**
 * get all user accounts
 * @param user
 */
module.exports.getUserProfile = async (user) => {
    const options = {
        method: 'GET',
        uri: userProfileServiceUrl.host + userProfileServiceUrl.getUserProfileEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Bearer ${user.token}`,
        },
        json: true,
        strictSSL: false
    };
    logger.info('Get user profile of the user');
    const result = await rp.get(options);
    if (!result.ok) {
        logger.error('Error in getting profile data' + result.message);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received profile data: ${JSON.stringify(result.data)}`);
    return result.data;
};

