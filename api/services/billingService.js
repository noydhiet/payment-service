/**
 * Created by Alif Septian N
 */

const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('billingService');
const config = require('../config');
const { InternalServerError } = require('../helpers/error');
const wrapper = require('../helpers/utils/wrapper');
const Authentication = require('../middleware/telkom_jwt');
const authentication = new Authentication();
const outstandingBillUrl = config.get('/billing').getOutstandingBill;
const billInfomationUrl = config.get('/billing').getBillDetails;
const ssoService = require('./ssoService');
const {BadRequestError} = require('../helpers/error');
const util = require('util');
let ejs = require("ejs");
let pdf = require("html-pdf");
let path = require("path");

module.exports.getBillHistory = async(indihomeNumber) => {

    const jwt = await authentication.getJWTFForUser();

    const ctx = 'queries-getAccessToken';
    const options = {
        method: 'POST',
        uri: outstandingBillUrl,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${jwt}`
        },
        body: {
            indihome_number: indihomeNumber
        },
        json: true,
        strictSSL: false,
    };
    try {
        const bills = await rp.post(options);
        if(!bills){
            logger.info(ctx,'results null error', 'API connection error');
            return {
                ok:false,
                status:500,
                message:'internal server error',
                data: {}
            };
        };

        if (!util.isNullOrUndefined(bills)) {

            if(bills.statusCode === '-2'){
                logger.error(`Error core api. Error: ${JSON.stringify(bills)}`);
                return {
                    ok:false,
                    status:409,
                    message:bills.returnMessage,
                    data: { }
                };
            }
            return {
                ok:true,
                status:200,
                message:'success fetching data',
                data: {
                    amount: bills.data.amount
                }
            };
        }
    } catch (err) {
        logger.info(ctx, err, 'API connection error');
        return wrapper.error(new InternalServerError('Core api error'));
    }
};

const getBillingInformation = async (indihomeNumber, period) => {
    const jwt = await authentication.getJWTFForUser();
    let transaction = 'MIHX-'+ Date.now();

    const ctx = 'queries-getAccessToken';
    const options = {
        method: 'POST',
        uri: billInfomationUrl,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${jwt}`
        },
        body: {
            EXTTransactionID:transaction,
            locale : 'en',
            serviceNumber: indihomeNumber
        },
        json: true,
        strictSSL: false
    };
    try {
        const bills = await rp.post(options);
        if(!bills){
            logger.info(ctx,'results null error', 'API connection error');
            return {
                ok:false,
                status:500,
                message:'internal server error',
                data: {}
            };
        };

        if (!util.isNullOrUndefined(bills)) {

            if(bills.statusCode === '-2'){
                logger.error(`Error core api. Error: ${JSON.stringify(bills)}`);
                return {
                    ok:false,
                    status:409,
                    message:bills.returnMessage,
                    data: { }
                };
            }

            let billList = bills.data.paymentSummaryList.paymentSummary;

            if(period){
                billList.forEach((element) => {
                    if(element.period === period.toString()){
                        billList = element;
                    }
                })
            }

            return {
                ok:true,
                status:200,
                message:'success fetching data',
                data: {
                    billDetails: billList
                }
            };
        };

    } catch (err) {
        logger.info(ctx, err, 'API connection error');
        return wrapper.error(new InternalServerError('API connection error'));
    }
}


module.exports.getBillInformation = async(indihomeNumber, period) => {
    return await getBillingInformation(indihomeNumber, period);
};

module.exports.requestBill = async (userId, indihomeNumber, period) => {
    const jwt = await authentication.getJWTFForUser();
    const userData = await ssoService.getUserSSOUserData(userId);
    if (!userData) {
        throw new BadRequestError('Invalid userId');
    }
    const billUrl = `http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com:8109/billing/print/${indihomeNumber}/${period}`;
    const message = `Please click on the following link for the requested bill \n ${billUrl}`;
    const json = {
        addressTO: userData.data.email,
        addressBCC: '',
        addressFROM: config.get('/billing').emailSenderAddress,
        addressCC: '',
        messageBody: message,
        subject: 'Monthly bill'
    };
    const options = {
        method: 'POST',
        uri: config.get('/billing').clientEmailSenderApi,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${jwt}`,
        },
        json,
        strictSSL: false
    };

    const result = await rp.post(options);
    if(result.statusCode ==="0"){
        return {
            ok:true,
            status:200,
            message: result.returnMessage,
            data: {}
        };
    }
    else
    {
        return {
            ok:false,
            status:409,
            message: result.returnMessage,
            data: {}
        };
    }
};

module.exports.generateBill = async(indihomeNumber, period, res) => {
    const bill =  await getBillingInformation(indihomeNumber, period);
    logger.info("outstanding bill dir:", path.join(__dirname + '../../../web/bill/outstanding-bill.html'));
    ejs.renderFile(path.join(__dirname + '../../../web/bill/outstanding-bill.html'), { billInfo: bill.data.billDetails }, (err, data) => {
    if (err) {
        logger.error("error finding html file", err);
        res.send(err);
    }
    else {
        let options = {
            "height": "11.25in",
            "width": "8.5in",
            "header": {
                "height": "20mm"
            },
            "footer": {
                "height": "20mm",
            },
        };
        pdf.create(data, options).toStream(function (err, stream) {
            if (err) {
                logger.error("error creating pdf", err);
                res.send(err);
            } else {
                res.setHeader('Content-Type', 'application/pdf');
                res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
                stream.pipe(res);
            }
        });
        }
    });
};



