const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('NotificationService');
const config = require('../config');
const {host, sendHomeNotificationEndpoint, getUserInboxEndpoint} = config.get('/userProfileService');
const {token} = config.get('/authorization');

module.exports.sendUserNotification = async (reqBody, userId) => {
    const options = {
        method: 'POST',
        uri: host + getUserInboxEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Basic ${token}`,
        },
        json: reqBody,
        strictSSL: false
    };
    //  send notifications
    logger.info(`sending notification to the user: ${JSON.stringify(options)}`);
    return rp(options)
        .then(response => {
            logger.info(`send notification to the user: ${userId}`);
            return response;
        })
        .catch(err => logger.error('error in sending notification', err));
};

module.exports.sendHomeNotification = async(reqBody, userId) => {

    const options = {
        method: 'POST',
        uri: `${host}${sendHomeNotificationEndpoint}`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Basic ${token}`,
        },
        json: reqBody,
        strictSSL: false
    };
    //  send notifications
    logger.info(`sending home notification to the user: ${JSON.stringify(options)}`);
    return rp(options)
        .then(response => {
            logger.info(`send home notification to the user: ${userId}`);
            return response;
        })
        .catch(err => logger.error('error in sending home notification', err));
};
