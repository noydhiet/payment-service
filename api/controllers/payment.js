/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */


const paymentService = require('../services/paymentService');
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES, SUCCESS, ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../helpers/error');
const _ = require('lodash');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('paymentController');
const validator = require('../validators/paymentValidators');
const JoiValidator = require('../helpers/utils/validator');

const {PREPAID_BILLING_TYPES, DEPOSIT_BILLING_TYPES, POSTPAID_BILLING_TYPES} = require('../helpers/utils/constants/paymentConstants');
const billingTypes = [PREPAID_BILLING_TYPES.SOD, PREPAID_BILLING_TYPES.MINI_PACKS, PREPAID_BILLING_TYPES.UPGRADE_SPEED,
    DEPOSIT_BILLING_TYPES.FULFILLMENT, POSTPAID_BILLING_TYPES.BILL];

exports.payment_deposit = (req, res) => {

    const depositOptions = ['permata','finpay','mandiri','bca','bri','bni','creditdebit','linkaja'];
    const {indiHomeNum, paymentType,amount,paymentCode} = req.body;
    const userData = req.userData;

    let paymentData = {};
    paymentData.indiHomeNum = indiHomeNum;
    paymentData.paymentType = paymentType;
    paymentData.paymentCode = paymentCode;
    paymentData.amount = amount;
    paymentData.type ='DEPOSIT';

    if(_.isEmpty(paymentType)){
        logger.error('Error. payment type was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment type was empty.')),
            'payment type cannot be empty', ERROR.BAD_REQUEST);
    }
    if(!depositOptions.includes(paymentType.toLowerCase())){
        logger.error('Error. invalid payment type.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payment type.')),
            'invalid payment type', ERROR.BAD_REQUEST);
    }
    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    if(_.isEmpty(paymentCode)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('paymentCode was empty.')),
            'paymentCode cannot be empty', ERROR.BAD_REQUEST);
    }


    paymentService.finpayPayment(paymentData,userData)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return  wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return  wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};


exports.payment_update = (req, res) => {

    const paymentStatusType = ['SUCCESS','FAILURE'];
    const {invoice,paymentStatus} = req.body;

    if(_.isEmpty(invoice)){
        logger.error('Error. invoice number was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invoice number was empty.')),
            'invoice number cannot be empty', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(paymentStatus)){
        logger.error('Error. payment status was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment status was empty.')),
            'payment status cannot be empty', ERROR.BAD_REQUEST);
    }

    if(!paymentStatusType.includes(paymentStatus.toUpperCase())){
        logger.error('Error. invalid payment status.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payment type.')),
            'invalid payment status', ERROR.BAD_REQUEST);
    }


    paymentService.updatePaymentStatus('other',invoice,paymentStatus)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};


exports.tmoney_payment_update = (req, res) => {

    const paymentStatusType = ['SUCCESS','FAILURE'];
    const {ticketId,paymentStatus} = req.body;
    const type = 'TMONEY';

    if(_.isEmpty(ticketId)){
        logger.error('Error. ticketId number was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('ticketId number was empty.')),
            'ticketId number cannot be empty', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(paymentStatus)){
        logger.error('Error. payment status was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment status was empty.')),
            'payment status cannot be empty', ERROR.BAD_REQUEST);
    }

    if(!paymentStatusType.includes(paymentStatus.toUpperCase())){
        logger.error('Error. invalid payment status.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payment type.')),
            'invalid payment status', ERROR.BAD_REQUEST);
    }

    paymentService.updatePaymentStatus(type,ticketId,paymentStatus)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return  wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};

exports.payment_postpaid = (req, res) => {

    const postPaidpaymentType = ['LINKAJA','CREDITDEBIT','SALDO'];
    const {indiHomeNum, paymentType,amount,paymentCode, pin} = req.body;
    const userData = req.userData;

    let paymentData = {};
    paymentData.indiHomeNum = indiHomeNum;
    paymentData.paymentType = paymentType;
    paymentData.paymentCode = paymentCode;
    paymentData.amount = amount;
    paymentData.pin = pin;
    paymentData.type ='POSTPAID';

    if(_.isEmpty(paymentType)){
        logger.error('Error. payment type was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment type was empty.')),
            'payment type cannot be empty', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(paymentCode)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('paymentCode was empty.')),
            'paymentCode cannot be empty', ERROR.BAD_REQUEST);
    }

    if(!postPaidpaymentType.includes(paymentType.toUpperCase())){
        logger.error('Error. invalid payment type.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payment type.')),
            'invalid payment type', ERROR.BAD_REQUEST);
    }

    if(paymentType.toLowerCase() === 'saldo' && !pin){
        logger.error('Error. invalid pin.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid pin.')),
            'invalid pin', ERROR.BAD_REQUEST);
    }

    paymentService.postpaidPayment(paymentData,userData)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return  wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return  wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};

exports.payment_prepaid = (req, res) => {

    const prepaidOptions = ['permata','finpay','mandiri','bca','bri','bni','creditdebit','linkaja','saldo'];

    const {indiHomeNum, paymentType, amount, paymentCode, pin} = req.body;
    const userData = req.userData;

    let paymentData = {};
    paymentData.indiHomeNum = indiHomeNum;
    paymentData.paymentType = paymentType;
    paymentData.paymentCode = paymentCode;
    paymentData.amount = amount;
    paymentData.pin = pin;
    paymentData.type ='PREPAID';

    if(_.isEmpty(paymentType)){
        logger.error('Error. payment type was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment type was empty.')),
            'payment type cannot be empty', ERROR.BAD_REQUEST);
    }

    if(!prepaidOptions.includes(paymentType.toLowerCase())){
        logger.error('Error. invalid payment type.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payment type.')),
            'invalid payment type', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }

    if(_.isEmpty(paymentCode)){
        logger.error('Error. paymentCode was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('paymentCode was empty.')),
            'paymentCode cannot be empty', ERROR.BAD_REQUEST);
    }

    if(paymentType.toLowerCase() === 'saldo'&& _.isEmpty(pin)){
        logger.error('Error.pin was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('pin was empty.')),
            'pin cannot be empty', ERROR.BAD_REQUEST);
    }


    paymentService.finpayPayment(paymentData, userData)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
           return wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};


exports.create_payment_code = (req, res) => {

    const {indiHomeNum, billItems, billingType, transactionId} = req.body;
    const userData = req.userData;

    let paymentData = {};
    paymentData.indiHomeNum = indiHomeNum;
    paymentData.items = billItems;
    paymentData.billingType = billingType;
    paymentData.transactionId = transactionId;

    if (_.isEmpty(billItems)) {
        logger.error('Error. payment items was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('payment items was empty.')),
            'payment items cannot be empty', ERROR.BAD_REQUEST);
    }

    if (_.isEmpty(indiHomeNum)) {
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    //  todo: billing type validation
    if (_.isEmpty(billingType)) {
        logger.error('Error. fullAmount was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('fullAmount was empty.')),
            'fullAmount cannot be empty', ERROR.BAD_REQUEST);
    }
    if (!billingTypes.includes(billingType)) {
        logger.error(`Error. invalid billing type: ${billingType}`);
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('fullAmount was empty.')),
            'invalid billing type.', ERROR.BAD_REQUEST);
    }
    if (_.isEmpty(transactionId)) {
        logger.error(`Error. transactionId was empty. ${req.body}`);
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('transactionId was empty.')),
            'transactionId cannot empty', ERROR.BAD_REQUEST);
    }
    paymentService.generatePaymentSession(paymentData, userData)
        .then(resp => {
            logger.info(`Successfully submitted payment request. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok, resp.status, resp.message, resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in payment request: ${new Date()}. Response: ${JSON.stringify(err)}`);
            // eslint-disable-next-line max-len
            return wrapper.formattedResponse(res, false, 500, err.message, {}, 500);
        });
};

/**
 * Get deposit payment data
 */
module.exports.getDepositInfo = async(req, res) => {
// get endpoint by indiHomeNum to filter a record having "type" : "DEPOSIT" && "status" : "SUCCESS"
    const indiHomeNum = req.query.indiHomeNum;
    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    paymentService.getDepositInfo(indiHomeNum)
        .then(resp => {
            logger.info(`Successfully received deposit data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in getting deposit data: ${new Date()}. Response: ${JSON.stringify(err)}`);
           return wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};

/**
 * Get monthly payment data
 */
module.exports.getMonthlyPaymentInfo = async(req, res) => {
// get endpoint by indiHomeNum to filter a record not having "type" : "DEPOSIT" && "status" : "SUCCESS"
    const indiHomeNum = req.query.indiHomeNum;
    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. indiHomeNum was empty.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum was empty.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    paymentService.getMonthlyPaymentInfo(indiHomeNum)
        .then(resp => {
            logger.info(`Successfully received monthly payment data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in getting monthly payment data: ${new Date()}. Response: ${JSON.stringify(err)}`);
           return wrapper.formattedResponse(res,false,500, err.message,{}, 500);

        });
};

/**
 * T-Money signup
 */
module.exports.tMoneySignUp = async(req, res) => {
    const payload = req.body;
    const validatePayload = JoiValidator.isValidPayload(payload, validator.tmoneyUserSignUpJoi);
    if(!validatePayload){
        logger.error('Error. invalid payload.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payload.')),
            'invalid payload', ERROR.BAD_REQUEST);
    }
    paymentService.tmoneyUserSignUp(payload, req.userData.userId)
        .then(resp => {
            logger.info(`Successfully changed pin. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in change pin: ${new Date()}. Response: ${JSON.stringify(err)}`);
           return wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error),
                `Error in change pin. error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * check T-Money user
 */
module.exports.checkUser = async(req, res) => {
    paymentService.checkTmoneyUserEmail(req.userData.userId)
        .then(resp => {
            logger.info(`User check success. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
           return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`User check failed: ${new Date()}. Response: ${JSON.stringify(err)}`);
          return  wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error),
                `User check failed. error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * change T-Money Pin
 */
module.exports.changeTmoneyPin = async(req, res) => {
    const payload = req.body;
    const validatePayload = JoiValidator.isValidPayload(payload, validator.changePinJoi);
    if(!validatePayload){
        logger.error('Error. invalid payload.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payload.')),
            'invalid payload', ERROR.BAD_REQUEST);
    }
    paymentService.changeTmoneyPin(payload, req.userData.userId)
        .then(resp => {
            logger.info(`Successfully changed pin. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return  wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in change pin: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return wrapper.formattedResponse(res,false,500, err.message,{}, 500);
        });
};

/**
 * reset T-Money Pin
 */
module.exports.resetTmoneyPin = async(req, res) => {
    paymentService.resetTmoneyPin(req.userData.userId)
        .then(resp => {
            logger.info(`Successfully reset pin. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return  wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in reset pin: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return wrapper.formattedResponse(res,false,500, err.message,{}, 500);
        });
};

/**
 * reset T-Money Pin
 */
module.exports.transferToLinkaja = async(req, res) => {
    const payload = req.body;
    const validatePayload = JoiValidator.isValidPayload(payload, validator.transferLinkaja);
    if(!validatePayload){
        logger.error('Error. invalid payload.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payload.')),
            'invalid payload', ERROR.BAD_REQUEST);
    }
    paymentService.transferToLinkaja(payload, req.userData.userId)
        .then(resp => {
            logger.info(`Successfully transferred to Linkaja. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            return wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in transfer to Linakaja: ${new Date()}. Response: ${JSON.stringify(err)}`);
            return wrapper.formattedResponse(res,false,500, err.message,{}, 500);
        });
};

/**
 * reset T-Money Pin
 */
module.exports.refundPayment = async(req, res) => {
    const payload = req.body;
    const validatePayload = JoiValidator.isValidPayload(payload, validator.refundPaymentJoi);
    if(!validatePayload){
        logger.error('Error. invalid payload.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payload.')),
            'invalid payload', ERROR.BAD_REQUEST);
    }

    paymentService.refundPayment(payload)
        .then(resp => {
            logger.info(`Successfully saved refund payment. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in save refund payment: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error),
                `Error in save refund payment. error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};


/**
 * update payment refund
 */
module.exports.updatePaymentRefund = async(req, res) => {
    const payload = req.body;
    const validatePayload = JoiValidator.isValidPayload(payload, validator.refundPaymentJoi);
    if(!validatePayload){
        logger.error('Error. invalid payload.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid payload.')),
            'invalid payload', ERROR.BAD_REQUEST);
    }

    paymentService.updatePaymentRefund(payload)
        .then(resp => {
            logger.info(`Successfully updated refund payment. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in updating refund payment: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error),
                `Error in save updating refund payment. error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * reset T-Money Pin
 */
module.exports.getRefundPayment = async(req, res) => {
    const invoiceNo = req.params.invoice;
    if(_.isEmpty(invoiceNo)){
        logger.error('Error. invalid invoice number.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('invalid invoice number.')),
            'invalid invoice number', ERROR.BAD_REQUEST);
    }

    paymentService.getRefundPayment(invoiceNo)
        .then(resp => {
            logger.info(`Successfully retrieved refund data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.formattedResponse(res, resp.ok,resp.status, resp.message,resp.data, SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in getting refund data: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error),
                `Error in getting refund data. error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};


