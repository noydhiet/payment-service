/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/10/2020
 */
const rewardService = require('../services/rewardService');
const wrapper = require('../helpers/utils/wrapper');
const {STATUS_TYPES, SUCCESS, ERROR } = require('../helpers/http-status/status_code');
const {BadRequestError} = require('../helpers/error');
const _ = require('lodash');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('rewardController');

/**
 * Get Points history data for user
 */
module.exports.getPointsExpiry = async (req, res) => {
    const selectedIndiHomeNum = req.params.indiHomeNum;

    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty selectedIndiHomeNum.')),
            'selectedIndiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getPointsExpiry(selectedIndiHomeNum)
        .then(resp => {
            logger.info(`Successfully received point expiry data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received point expiry data successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get point expiry data. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get point expiry data. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Get Points history data for user
 */
module.exports.getPointsHistory = async (req, res) => {
    const selectedIndiHomeNum = req.params.indiHomeNum;

    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty selectedIndiHomeNum.')),
            'selectedIndiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getPointsHistory(selectedIndiHomeNum)
        .then(resp => {
            logger.info(`Successfully received point history data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received point history data successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get point history data. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get point history data. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Get Points redeem history data for user
 */
module.exports.getPointsRedeemHistory = async (req, res) => {
    const selectedIndiHomeNum = req.params.indiHomeNum;
    const userId = req.userData.userId;

    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty selectedIndiHomeNum.')),
            'selectedIndiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getPointsRedeemHistory(selectedIndiHomeNum, userId)
        .then(resp => {
            logger.info(`Successfully received point redeem history data. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received point redeem history data successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get point redeem history data. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get point redeem history data. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Check Points summary for indiHome Number
 */
module.exports.checkPoints = async (req, res) => {
    const selectedIndiHomeNum = req.params.indiHomeNum;

    if(_.isEmpty(selectedIndiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty selectedIndiHomeNum.')),
            'selectedIndiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.checkPoints(selectedIndiHomeNum)
        .then(resp => {
            logger.info(`Successfully received point summary. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received point summary successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get point summary. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get point summary. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Get rewards lists
 */
module.exports.getRewardsList = async (req, res) => {
    const indiHomeNum = req.params.indiHomeNum;
    let filterBy, group;
    const userData = req.userData;

    if(req.query && req.query.filterBy){
        filterBy = req.query.filterBy;
    }
    if(req.query && req.query.group){
        group = req.query.group;
    }

    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty indiHomeNum.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getRewardsList(indiHomeNum, userData, filterBy, group)
        .then(resp => {
            if(_.isEmpty(resp.data)) {
                logger.info(`No matching rewards found. Date: ${new Date()}.`);
                return wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'No matching rewards found.', SUCCESS.OK);
            }
            logger.info(`Successfully received rewards list. Date: ${new Date()}. Response length: ${resp.length}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received rewards list successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get rewards list. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get rewards list. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Get recommended rewards lists
 */
module.exports.getRecommendedRewards = async (req, res) => {
    const indiHomeNum = req.params.indiHomeNum;
    const userData = req.userData;

    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty indiHomeNum.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getRecommendedRewards(indiHomeNum, userData)
        .then(resp => {
            if(_.isEmpty(resp.data)) {
                logger.info(`No matching recommended rewards found. Date: ${new Date()}.`);
                return wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'No matching recommended rewards found.', SUCCESS.OK);
            }
            logger.info(`Successfully received recommended rewards list. Date: ${new Date()}. Response length: ${resp.length}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received recommended rewards list successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get recommended rewards list. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get recommended rewards list. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Get point details by point Id
 */
module.exports.getPointDetail = async (req, res) => {
    const indiHomeNum = req.query.indiHomeNum;
    const pointId = req.query.pointId;

    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('Empty indiHomeNum.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.getPointDetails(pointId, indiHomeNum)
        .then(resp => {
            logger.info(`Successfully received point details. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Received point details successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get point detail. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err.error), `Error in get point detail. Error: ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Redeem points
 */
module.exports.redeemPoints = async (req, res) => {
    const {indiHomeNum, pointId, redeemKey} = req.body;
    const redeemData = {
        pointId, redeemKey
    };
    const userData = req.userData;
    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum messages.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }
    rewardService.redeemPoints(redeemData, indiHomeNum, userData)
        .then(resp => {
            logger.info(`Successfully redeemed points. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Redeemed successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in redeem points. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in redeeming points. ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Get rewards types
 */
module.exports.getRewardsTypes = async (req, res) => {
    rewardService.getRewardsTypes()
        .then(resp => {
            logger.info(`Successfully received reward types. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully received reward types', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get reward types. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in get reward types. ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Get reward type
 */
module.exports.getRewardsType = async (req, res) => {
    const id = req.params.id;
    rewardService.getRewardsType(id)
        .then(resp => {
            logger.info(`Successfully received reward type. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully received reward type', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get reward type. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in get reward type. ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Add new rewards types
 */
module.exports.addRewardsType = async (req, res) => {
    const payload =  req.body;
    rewardService.addRewardsType(payload)
        .then(resp => {
            logger.info(`Successfully added reward type. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully added reward type', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in adding reward type. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in adding reward type. ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Update rewards types
 */
module.exports.updateRewardsType = async (req, res) => {
    const payload =  req.body;
    const id = req.params.id;
    rewardService.updateRewardsType(payload, id)
        .then(resp => {
            logger.info(`Successfully updated reward type. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully updated reward type', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in updating reward type. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in updating reward type. ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Get vouchers
 */
module.exports.getVouchers = async (req, res) => {
    const indiHomeNum = req.query.indiHomeNum;
    const userData = req.userData;
    rewardService.getVouchers(indiHomeNum, userData)
        .then(resp => {
            if(_.isEmpty(resp)) {
                logger.info(`No vouchers received. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
                return wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'No vouchers received.', SUCCESS.OK);
            }
            logger.info(`Successfully received vouchers. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully received vouchers', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in get vouchers. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in get vouchers. ${err.message}`, 200, wrapper.checkErrorCode(err));

        });
};

/**
 * Use vouchers
 */
module.exports.useVoucher = async (req, res) => {
    const {indiHomeNum, voucherId, redeemKey} = req.body;
    const voucherData = {
        voucherId, redeemKey
    };
    const userData = req.userData;
    if(_.isEmpty(indiHomeNum)){
        logger.error('Error. Empty indiHomeNum.');
        return wrapper.response(res, STATUS_TYPES.FAIL, wrapper.error(new BadRequestError('indiHomeNum messages.')),
            'indiHomeNum cannot be empty', ERROR.BAD_REQUEST);
    }

    rewardService.useVoucher(voucherData, indiHomeNum, userData)
        .then(resp => {
            logger.info(`Successfully used voucher. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Used voucher successfully', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in use voucher. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in use voucher. ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Activate rewards
 */
module.exports.activateRewards = async (req, res) => {
    const {indiHomeNum, selectedTypes} = req.body;

    rewardService.activateRewards(indiHomeNum, selectedTypes, req.userData)
        .then(resp => {
            logger.info(`Successfully activated rewards. Date: ${new Date()}. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(resp), 'Successfully activated rewards.', SUCCESS.OK);
        })
        .catch(err => {
            logger.error(`Error in rewards activation. Date: ${new Date()}. Response: ${JSON.stringify(err)}`);
            wrapper.response(res, STATUS_TYPES.SUCCESS, wrapper.data(err), `Error in rewards activation. ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};
