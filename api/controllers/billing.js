/**
 * Created by Alif Septian N
 */

const billingService = require('../services/billingService');
const wrapper = require('../helpers/utils/wrapper');
const { SUCCESS:http } = require('../helpers/http-status/status_code');

module.exports.billHistory = async(req, res) => {
    const { indihomeNumber } = req.params;
    const result = await billingService.getBillHistory(indihomeNumber);
    if(result.err){
        return wrapper.response(res, 'fail', result);
    }
    return wrapper.formattedResponse(res, result.ok,result.status, result.message,result.data, 200);
}


module.exports.billInformation = async(req, res) => {
    const { indihomeNumber,period} = req.params;
    const result = await billingService.getBillInformation(indihomeNumber,period);
    if(result.err){
        return wrapper.response(res, 'fail', result);
    }
    return wrapper.formattedResponse(res, result.ok,result.status, result.message,result.data, 200);
}

module.exports.requestBill = async(req, res) => {
    const { indihomeNumber} = req.params;
    const period = req.query.period;
    const result = await billingService.requestBill(req.userData.userId, indihomeNumber, period);
    if(result.err){
        return wrapper.response(res, 'fail', result);
    }
    return wrapper.formattedResponse(res, result.ok,result.status, result.message,result.data, 200);
}

module.exports.generateBill = async(req, res) => {
    const { indihomeNumber} = req.params;
    const period = req.query.period;
    await billingService.generateBill(indihomeNumber, period, res);
}
