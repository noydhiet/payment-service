
/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */

module.exports = (req, res, next) => {
    try {

      const auth = {login: process.env.BASIC_AUTH_USERNAME, password:process.env.BASIC_AUTH_PASSWORD}

      const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
      const [login, password] = new Buffer(b64auth, 'base64').toString().split(':')

      if (login && password && login === auth.login && password === auth.password) {
        // Access granted...
        return next()
      }else{
        return res.status(401).json({
          ok:false,
          status:401,
          message: 'Basic Auth failed',
          data:{}
      });

      }

    } catch (error) {
        return res.status(401).json({
            ok:false,
            status:401,
            message: 'Basic Auth failed',
            data:{}
        });
    }
};