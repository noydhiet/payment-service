/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */


const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {

        console.log(process.env.SHARED_SECRET);
        const token = req.headers.authorization.split(' ')[1];
        const decoded = jwt.verify(token,process.env.SHARED_SECRET);
        req.userData = decoded;
        req.userData.token = token;
        next();
    } catch (error) {
       
        if (error.name == 'TokenExpiredError') {
            return res.status(200).json({
                ok:false,
                status:402,
                message: 'jwt expired',
                data:{}
            });
        }
        return res.status(200).json({
            ok:false,
            status:401,
            message: 'Auth failed',
            data:{}
        });
        
    }
};