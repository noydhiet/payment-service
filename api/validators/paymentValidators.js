const joi = require('joi');

const tmoneyUserSignUpJoi = joi.object({
    password: joi.string().required(),
    pin: joi.string().required()
});

const changePinJoi = joi.object({
    oldPin: joi.string().required(),
    newPin: joi.string().required()
});

const transferLinkaja = joi.object({
    productCode: joi.string().required(),
    amount: joi.string().required(),
    linkajaMobileNumber: joi.string().required(),
    pin: joi.string().required(),
    transactionID: joi.string().required(),
    refNo: joi.string().required()
});

const postPaidPayment = joi.object({
    amount: joi.string().required(),
    billNumber: joi.string().required(),
    pin: joi.string().required()
});

const refundPaymentJoi = joi.object({
    invoiceNo: joi.string().required(),
    amount: joi.string().required(),
    type: joi.string().required(),
    status: joi.string().required(),
    reason: joi.string().required(),
    processedDate: joi.string().required()
});

const updateRefundPaymentJoi = joi.object({
    invoiceNo: joi.string().required(),
    accountHolder: joi.string().required(),
    bank: joi.string().required(),
    accountNumber: joi.string().required()
});

module.exports = {
    changePinJoi,
    transferLinkaja,
    tmoneyUserSignUpJoi,
    refundPaymentJoi,
    updateRefundPaymentJoi,
    postPaidPayment
}
