/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */

const express = require('express');
const router = express.Router();

const paymentController = require('../controllers/payment');
const basicAuth = require('../middleware/basic_auth');
const checkAuth = require('../middleware/check-auth');
const path = require("path");

router.get('/deposit',basicAuth, paymentController.getDepositInfo);
router.get('/monthly',basicAuth, paymentController.getMonthlyPaymentInfo);
router.post('/deposit',checkAuth, paymentController.payment_deposit);
router.post('/prepaid',checkAuth, paymentController.payment_prepaid);
router.post('/postpaid',checkAuth, paymentController.payment_postpaid);
router.post('/session',checkAuth, paymentController.create_payment_code);
router.put('/status',basicAuth, paymentController.payment_update);
router.get('/paymentStatus', function(request, response){
    response.sendFile(path.join(__dirname + '../../../web/payment/payment-status.html'));
});

//T-Money
router.put('/tmoney/status/',basicAuth, paymentController.tmoney_payment_update);
router.post('/tmoney/signUp', checkAuth, paymentController.tMoneySignUp);
router.get('/tmoney/emailCheck', checkAuth, paymentController.checkUser);
router.post('/tmoney/changePin', checkAuth, paymentController.changeTmoneyPin);
router.post('/tmoney/resetPin', checkAuth, paymentController.resetTmoneyPin);
router.post('/tmoney/transferToLinkaja', checkAuth, paymentController.transferToLinkaja);

//refund
router.post('/refund', basicAuth, paymentController.refundPayment);
router.put('/refund', basicAuth, paymentController.updatePaymentRefund);
router.get('/refund/:invoice', checkAuth, paymentController.getRefundPayment);

module.exports = router;


