/**
 * Created by Alif Septian N
 */

const express = require('express');
const router = express.Router();

const billingController = require('../controllers/billing');
const checkAuth = require('../middleware/check-auth');

router.get('/outstanding/:indihomeNumber', checkAuth, billingController.billHistory);
router.get('/infomation/:indihomeNumber/:period', checkAuth, billingController.billInformation);
router.get('/requestBill/:indihomeNumber', checkAuth, billingController.requestBill);
router.get('/print/:indihomeNumber', billingController.generateBill);

module.exports = router;
