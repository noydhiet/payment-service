/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/10/2020
 */

const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const basicAuth = require('../middleware/basic_auth');

const RewardController = require('../controllers/reward');

// Vouchers
router.get('/vouchers', checkAuth, RewardController.getVouchers);
router.post('/vouchers', checkAuth, RewardController.useVoucher);

// Rewards Types
router.get('/types', checkAuth, RewardController.getRewardsTypes);
router.post('/types', basicAuth, RewardController.addRewardsType);
router.get('/types/:id', basicAuth, RewardController.getRewardsType);
router.put('/types/:id', basicAuth, RewardController.updateRewardsType);

// Rewards and points
router.post('/points', checkAuth, RewardController.redeemPoints);
router.get('/point', checkAuth, RewardController.getPointDetail);
router.get('/points/:indiHomeNum', checkAuth, RewardController.checkPoints);
router.get('/:indiHomeNum', checkAuth, RewardController.getRewardsList);
router.get('/recommended/:indiHomeNum', checkAuth, RewardController.getRecommendedRewards);
router.get('/points/redeem/history/:indiHomeNum', checkAuth, RewardController.getPointsRedeemHistory);
router.get('/points/history/:indiHomeNum', checkAuth, RewardController.getPointsHistory);
router.get('/points/expiry/:indiHomeNum', checkAuth, RewardController.getPointsExpiry);
router.post('/', checkAuth, RewardController.activateRewards);

module.exports = router;
