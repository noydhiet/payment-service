FROM node:12.14.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install --silent
RUN npm audit fix
COPY . /usr/src/app

CMD ["npm", "start"]
