/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Saminda Kularathne
 */

const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const paymentRoutes = require('./api/routes/payment');
const rewardsRoutes = require('./api/routes/reward');
const billingRoutes = require('./api/routes/billing');
const swaggerUi = require('swagger-ui-express');
const esClient = require('./api/helpers/databases/elasticSearch/client');

const cors = require('cors');
const fs = require('fs');
const path = require('path');
const spec = fs.readFileSync(path.join(__dirname, './swagger.yaml'), 'utf8');

const jsyaml = require('js-yaml');
const swaggerDocument = jsyaml.safeLoad(spec);

const Logger = require('./api/helpers/utils/logger');
const logger = new Logger('paymentService');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// To check the ES connection

esClient.ping({
    // ping usually has a 3000ms timeout
    requestTimeout: 1000
}, (error) => {
    if (error) {
        logger.error(`Elasticsearch cluster is down!. Error: ${JSON.stringify(error)}`);

    } else {
        logger.info('Elastic search connected');
    }
});


app.use('/payment', paymentRoutes);
app.use('/rewards', rewardsRoutes);
app.use('/billing', billingRoutes);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    logger.debug('App crashed');
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;
