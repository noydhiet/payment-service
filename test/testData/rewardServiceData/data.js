/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/20/2020
 */
module.exports = {
    userData: {
        email: "nobin272@gmail.com",
        userId: "5e4b55ebf819670012ed26d3",
        userRole: [
            "generalUser"
        ],
        mobile: "123213124",
        iat: 1573634638,
        exp: 1573638238
    },
    indiHomeNum: '121105202690',
    indiHomeNumForRedeemHistory: '02148182147',
    indiHomeNumForHistory: '122302255071',
    invalidIndiHomeNum: 'invalid',
    pointData: {
        pointId: '167',

    },
    redeemData: {
        pointId: '167',
        redeemKey: 'U8A3ABS7'
    },
    voucherData: {
        voucherId: '452706',
        redeemKey: 'HJQTZLUM'
    },
    rewardsPreference: [
        "AUTOMOTIVE", "E-COMMERCE", "PRIZE"
    ],
    rewardsType: {
        id: "reward_cat_000",
        name: "test reward type",
        active: true
    },
    invalidRewardsType: {
        name: "test reward type",
        active: true
    },
};