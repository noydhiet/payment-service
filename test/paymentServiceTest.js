/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/20/2020
 */
const paymentService = require('../api/services/paymentService');
const testData = require('./testData/rewardServiceData/data');
let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Payment service', function () {
    this.timeout(100000);

    describe('Get Deposit payment data for user', () => {
        it('Should get Deposit payment data successfully.', function () {
            this.timeout(100000);
            let response = paymentService.getDepositInfo(testData.indiHomeNum);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get Deposit payment data. Null params', function () {
            this.timeout(100000);
            let response = paymentService.getDepositInfo(null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get Deposit payment data. Empty params', function () {
            this.timeout(100000);
            let response = paymentService.getDepositInfo('');
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get Deposit payment data. Invalid params', function () {
            this.timeout(100000);
            let response = paymentService.getDepositInfo(testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });


    describe('Get monthly payment data for user', () => {
        it('Should get Deposit monthly data successfully.', function () {
            this.timeout(100000);
            let response = paymentService.getMonthlyPaymentInfo(testData.indiHomeNum);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get monthly payment data. Null params', function () {
            this.timeout(100000);
            let response = paymentService.getMonthlyPaymentInfo(null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get monthly payment data. Empty params', function () {
            this.timeout(100000);
            let response = paymentService.getMonthlyPaymentInfo('');
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get monthly payment data. Invalid params', function () {
            this.timeout(100000);
            let response = paymentService.getMonthlyPaymentInfo(testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });
});