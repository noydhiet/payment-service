/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/20/2020
 */
const rewardService = require('../api/services/rewardService');
const {REWARDS_TYPES} = require('../api/helpers/utils/constants/rewardConstants');
const testData = require('./testData/rewardServiceData/data');
const config = require('../api/config');
const rp = require('request-promise');
let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Reward service', function () {
    this.timeout(100000);
    const user = testData.userData;
    before(() => {
        this.timeout(100000);
        const options = {
            method: 'POST',
            uri: 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com/user/login',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                Authorization: 'Basic ' + config.get('/authorization').token,
            },
            json: {
                "email": "pccw@dev.com",
                "password": "dev123"
            },
            strictSSL: false
        };
        const result = rp.post(options);

        // set user token
        return result.then(out => {
            user.token = out.data.token;
        }).catch(()=> {});
    });
    describe('Get Points history data for user', () => {
        it('Should get points history successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getPointsHistory(testData.indiHomeNumForHistory);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get points history. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsHistory(null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get points history. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsHistory(testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get Points expiry data for user', () => {
        it('Should get points expiry successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getPointsExpiry(testData.indiHomeNumForHistory);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get points expiry. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsExpiry(null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get points expiry. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsExpiry(testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get Points redeem history data for user', () => {
        it('Should get points redeem history successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getPointsRedeemHistory(testData.indiHomeNumForRedeemHistory, testData.userData.userId);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get points redeem history. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsRedeemHistory(null, testData.userData.userId);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get points redeem history. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getPointsRedeemHistory(testData.invalidIndiHomeNum, testData.userData.userId);
            return response.catch(data => {
                expect(data).not.to.be.empty;
            });
        });
    });

    describe('Check points', () => {
        it('Should get points List successfully.', function () {
            this.timeout(100000);
            let response = rewardService.checkPoints(testData.indiHomeNumForHistory);
            return response.then(data => {
                expect(data.data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
                expect(data.data.totalPoints).not.to.be.empty;
                expect(data.data.pointsCurrentMonth).not.to.be.empty;
                expect(data.data).to.have.property('latestExpiry');
            });
        });

        it('Should fail to get points List. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.checkPoints(null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get points List. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.checkPoints(testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Activate rewards', () => {
        it('Should Activate rewards successfully.', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(testData.indiHomeNum, testData.rewardsPreference, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to Activate rewards. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards('', testData.rewardsPreference, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to Activate rewards. Null params', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(null, testData.rewardsPreference, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to Activate rewards. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(testData.indiHomeNum, [], user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to Activate rewards. Invalid field values', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(testData.indiHomeNum, ["test"], user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to Activate rewards. Invalid param type', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(testData.indiHomeNum, "test", user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to Activate rewards. Invalid user', function () {
            this.timeout(100000);
            let response = rewardService.activateRewards(testData.indiHomeNum, testData.rewardsPreference, {});
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get rewards list', () => {
        it('Should get rewards list successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should get filtered rewards list successfully.', function () {
            this.timeout(100000);
            REWARDS_TYPES.forEach(type => {
                let response = rewardService.getRewardsList(testData.indiHomeNum, user, type);
                return response.then(data => {
                    expect(data.data).to.be.an('array');
                    expect(data).not.to.be.empty;
                });
            });
        });

        it('Should get grouped rewards list successfully. group: true, ALL ', function () {
            this.timeout(100000);
            REWARDS_TYPES.forEach(type => {
                let response = rewardService.getRewardsList(testData.indiHomeNum, user, type, 'true');
                return response.then(data => {
                    expect(data.data).to.be.an('object');
                    expect(data).not.to.be.empty;
                });
            });
        });

        it('Should get grouped rewards list successfully. group: true, One', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, REWARDS_TYPES[1], 'true');
            return response.then(data => {
                expect(data.data).to.be.an('object');
                expect(data).not.to.be.empty;
            });
        });

        it('Should get ungrouped rewards list successfully. group: false', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, REWARDS_TYPES[1], 'false');
            return response.then(data => {
                expect(data.data).to.be.an('array');
                expect(data).not.to.be.empty;
            });
        });

        it('Should get ungrouped rewards list successfully. group: false, no filter', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, undefined, 'false');
            return response.then(data => {
                expect(data.data).to.be.an('array');
                expect(data).not.to.be.empty;
            });
        });

        it('Should get grouped rewards list successfully. group: true, no filter', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, undefined, 'true');
            return response.then(data => {
                expect(data.data).to.be.an('object');
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get grouped rewards list. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, REWARDS_TYPES[3], 'invalid');
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get filtered rewards list. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, '');
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get filtered rewards list. Null params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.indiHomeNum, user, null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get filtered rewards list. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.invalidIndiHomeNum, user, 'invalid');
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get rewards list. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(null, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get rewards list. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getRewardsList(testData.invalidIndiHomeNum, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get recommended rewards list', () => {
        it('Should get recommended rewards list successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getRecommendedRewards(testData.indiHomeNum, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get recommended rewards list. Null params', function () {
            this.timeout(100000);
            let response = rewardService.getRecommendedRewards(null, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get recommended rewards list. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getRecommendedRewards('', user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get recommended rewards list. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.getRecommendedRewards(testData.invalidIndiHomeNum, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get point details', () => {
        it('Should get point detail successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getPointDetails(testData.pointData.pointId, testData.indiHomeNum);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to get point detail. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.getPointDetails(testData.pointData.pointId, null);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get point detail. Empty params', function () {
            this.timeout(100000);
            const invalidData = Object.assign({}, testData.pointData);
            invalidData.pointId = '';
            let response = rewardService.getPointDetails(invalidData.pointId, testData.indiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to get point detail. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.checkPoints(testData.pointData.pointId, testData.invalidIndiHomeNum);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Redeem points', () => {
        it('Should redeem points successfully.', function () {
            this.timeout(100000);
            let response = rewardService.redeemPoints(testData.redeemData, testData.indiHomeNum, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to redeem points. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.redeemPoints(testData.redeemData, null, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to redeem points. Empty params', function () {
            this.timeout(100000);
            const invalidData = Object.assign({}, testData.redeemData);
            invalidData.pointId = '';
            let response = rewardService.redeemPoints(invalidData, testData.indiHomeNum, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to redeem points. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.redeemPoints(testData.redeemData, testData.invalidIndiHomeNum, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Reward Types CRUD', () => {
        describe('Add reward types', () => {
            it('Should add reward type successfully.', function () {
                this.timeout(100000);
                let response = rewardService.addRewardsType(testData.rewardsType);
                return response.then(data => {
                    expect(data).not.to.be.empty;
                });
            });
            it('Should fail to add reward type. Invalid Object.', function () {
                this.timeout(100000);
                let response = rewardService.addRewardsType(testData.invalidRewardsType);
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
            it('Should fail to add reward type. Empty Object.', function () {
                this.timeout(100000);
                let response = rewardService.addRewardsType({});
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
        });
        describe('Get reward types', () => {
            it('Should get reward types successfully.', function () {
                this.timeout(100000);
                let response = rewardService.getRewardsTypes();
                return response.then(data => {
                    expect(data.data).not.to.be.empty;
                });
            });
            it('Should get reward type by id successfully.', function () {
                this.timeout(100000);
                let response = rewardService.getRewardsType(testData.rewardsType.id);
                return response.then(data => {
                    expect(data.data).not.to.be.empty;
                });
            });
            it('Should fail to get reward type. Empty param.', function () {
                this.timeout(100000);
                let response = rewardService.getRewardsType('');
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
            it('Should fail to get reward type. Null param.', function () {
                this.timeout(100000);
                let response = rewardService.getRewardsType(null);
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
        });

        describe('Update reward types', () => {
            it('Should update reward type successfully.', function () {
                this.timeout(100000);
                let response = rewardService.updateRewardsType({active: false}, testData.rewardsType.id);
                return response.then(data => {
                    expect(data).not.to.be.empty;
                });
            });
            it('Should fail to update reward type. Invalid Object.', function () {
                this.timeout(100000);
                let response = rewardService.updateRewardsType({id: testData.rewardsType.id}, testData.rewardsType.id);
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
            it('Should fail to update reward type. Null param.', function () {
                this.timeout(100000);
                let response = rewardService.updateRewardsType({active: false}, null);
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
            it('Should fail to update reward type. Empty param.', function () {
                this.timeout(100000);
                let response = rewardService.updateRewardsType({active: false}, '');
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });

            it('Should fail to update reward type. Empty Object.', function () {
                this.timeout(100000);
                let response = rewardService.updateRewardsType({}, testData.rewardsType.id);
                return response.catch(data => {
                    expect(data).to.be.empty;
                });
            });
        });
    });

    describe('Use voucher', () => {
        it('Should Use voucher successfully.', function () {
            this.timeout(100000);
            let response = rewardService.useVoucher(testData.voucherData, testData.indiHomeNum, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to Use voucher. Empty params', function () {
            this.timeout(100000);
            let response = rewardService.useVoucher(testData.voucherData, null, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to Use voucher. Empty params', function () {
            this.timeout(100000);
            const invalidData = Object.assign({}, testData.voucherData);
            invalidData.voucherId = '';
            let response = rewardService.useVoucher(invalidData, testData.indiHomeNum, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to Use voucher. Invalid params', function () {
            this.timeout(100000);
            let response = rewardService.useVoucher(testData.voucherData, testData.invalidIndiHomeNum, user,);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Get vouchers for user', () => {
        it('Should get vouchers successfully.', function () {
            this.timeout(100000);
            let response = rewardService.getVouchers(testData.indiHomeNum, user);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });
        it('Should fail to get vouchers. Null param', function () {
            this.timeout(100000);
            let response = rewardService.getVouchers(null, user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to get vouchers. Empty param', function () {
            this.timeout(100000);
            let response = rewardService.getVouchers('', user);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('Should fail to get vouchers. Invalid user data', function () {
            this.timeout(100000);
            let response = rewardService.getVouchers(testData.indiHomeNum, {token : 'invalid'});
            return response.catch(data => {
                expect(data).not.to.be.empty;
            });
        });
    });
});
